var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(request, response) {
    return response.status(200).send();
});

/**
 * Payement validation 
 */
router.post('/validation', (request, response) => {

    random = (Math.random() * 100).toFixed();
    bankValidation = (random >= 80) ? true : false;

    return response.json({
        "invoice": request.body.invoice,
        "validation" : bankValidation
    })
});

module.exports = router;