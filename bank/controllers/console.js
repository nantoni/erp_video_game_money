const chalk = require('chalk');

/**
 * Setup message coloration
 */
var message = {

    /**
     * Display info message
     * @param {*} tag : tag
     * @param {*} content : message
     */
    info: (tag, content) => {
        return console.log(chalk.blue(tag) + ": "+ content);
    },

    /**
     * Display error message
     * @param {*} tag : tag
     * @param {*} content : message 
     */
    error: (tag, content) => {
        return console.log(chalk.red(tag) + ": "+ content);
    },

    /**
     * Display warning message
     * @param {*} tag : tag
     * @param {*} content : message
     */
    warning: (tag, content) => {
        return console.log(chalk.yellow(tag) + ": "+ content);
    },

    /**
     * Dsiplay debug message
     * @param {*} tag : tag
     * @param {*} content : message
     */
    debug: (tag, content) => {
        return console.log(chalk.cyan(tag) + ": "+ content);
    }
}

module.exports = message;