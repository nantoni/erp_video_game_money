# [ERP_Video_Game_Money](http://51.178.140.210/)

Projet en ligne dispoible à cette adresse : __http://vps-8ac7aacc.vps.ovh.net/accueil__   
Code et README disponible à cette adresse : __https://gitlab.com/nantoni/erp_video_game_money__  

## Organisation du projet
 
- Responsable infrastructure : Nathan
- Développeurs front : Corentin
- Développeurs back : Anthony, François, Lucas  
- Concepteur de la base de données : Lucas
- Testing master : François
- Rédaction Readme et documents annexes : Carole et Nathan

## Introduction

>Dans le cadre du module “Processus métiers et ERP” dispensé au CNAM, ce TP a pour objectif de développer le module comptabilité d’un ERP. Celui-ci doit dialoguer avec le module de l’équipe GAMERS chargée de gérer les joueurs abonnés et de nous transmettre les factures mensuelles générées.
 
__Sujet :__ Simulation de l’activité commerciale d’une société de Vidéo Game en Ligne
G. Lacreuse
 
ERP : TP de réalisation de modules fonctionnels interconnectés avec une problématique sur 2 éléments :
- Gestion des comptes clients et de leur dette / situation
- Génération automatique d’écritures comptables et limitation des frais de saisie.
 
Module : Création d'une application MONEY de comptabilité : 
Génération d'écritures comptables générales et auxiliaires sur la base des facturations mensuelles
Création d’une interface de visualisation de ces écritures
Intégration des relevés bancaires
Transmission au groupe 1 des règlements
 
## Stratégie d’échange de données 
 
Chaque sous-équipe a fait le choix de créer une API REST pour communiquer.
 
__Qu’est-ce qu’une API ?__
 
API REST (Representational State Transfer Application Program Interface) est un style architectural qui permet aux logiciels de communiquer entre eux sur un réseau ou sur un même appareil. Le plus souvent les développeurs utilisent des API REST pour créer des services web. Souvent appelés services web RESTful, REST utilise des requêtes HTTP pour récupérer et publier des données entre un client et un serveur. Celles-ci sont souvent au format JSON.
 
## Structure du projet
![](echanges-Technique.png)


### Serveur
 
Nous utilisons NginX comme serveur web, il est hébergé sur un serveur virtuel OVH avec un processeur monocoeur et 2 Gib de mémoire vive 
 
### Frontend
 
Pour l’interface utilisateur nous avons une application web basée sur le framework Angular.
Elle est hébergée à cette addresse : http://vps-8ac7aacc.vps.ovh.net/ 
 
Elle requête une API privée en local pour obtenir les écritures comptables stockées en base de données. 
 
### Backend 
 
Pour le backend nous avons un serveur Express.js basé sur la plateforme NodeJS. Celui-ci intercepte les requêtes sur le port 4003.
 
### Reverse proxy
 
Un reverse proxy s’occupe de rediriger les requêtes sur le port 4000 vers le port 4003. Il a été mis en place car d’autres services sont hébergés sur le même serveur et ils doivent tous être accessibles via le port 4000. Le reverse proxy permet de rediriger les requêtes vers différents ports en fonction du nom de domaine avec lequel est faite la requête.
 
### Base de donnée 
La base de données est de type MySQL (port 3306). Son accès est facilité via l'utilisation d’un ORM node.js appelé Sequelize.

## Route exposée par l’API du module Gamers


### Simulation du paiement de la banque

L’équipe GAMERS nous expose la route /setPayements dans le but de leur transmettre les factures payées. L’information nous est transmise par la simulation du paiement de la banque.
La validation de la banque est simulée via un cron (script) exécuté chaque minute. Son rôle est de simuler de façon aléatoire des paiements parmi la liste des factures réceptionnées.
Le cron est exécuté sur le serveur backend de l’ERP. Celui-ci fera un appel à un Webservice vers le serveur de la banque, pour valider ou non un paiement. 

Les paramètres suivants sont requis :  
invoiceId : Numéro de facture ayant été payée  
date : Date de paiement  
 
__POST : http://cnamerp.leojullerot.fr:4689/setPayments__   
```
{
    "invoiceId" : 20,
    "date" : "17/01/2020"
}
```
## Route exposé au SI du groupe GAMERS par notre API 
 
### Réception des factures mensuelles
Notre API expose la route /invoices en POST dans le but de réceptionner les facturations mensuelles générées par l’équipe GAMERS. Nous avons identifié cinq propriétés pertinentes à la génération de nos écritures comptables :  
invoiceId : Référence de la facture  
clientId : Identifiant du client  
date : date d'émission de la facture  
amount : montant TTC (Toutes Taxes Comprises) de la facture  
VAT? : (optionel) taux de TVA par défaut fixé à 20%, sauf si un taux spécifique nous est communiqué  

__POST : [domain]:4000/v1/invoices__
```
[
    {
        "invoiceId": number,
        "clientId": number,
        "date": Date,
        "amount": number
        "VAT"?: number
    },
...
]
```

A la réception, les factures sont transformées en deux types d’écritures comptables : les générales et auxiliaires

## Routes internes à notre SI pour afficher les données en base depuis le front  
### Comptabilité générale
__GET: [domain]:4000/v1/invoices__
Cette route /invoices permet de de renvoyer les écritures comptables générales sur la base des facturations mensuelles. Elles sont ensuite affichées sur l’interface de visualisation.

### Comptabilité auxiliaires
__GET: [domain]:4000/v1/invoices/auxiliar__  
La route /invoices/auxiliar permet de renvoyer les écritures comptables auxiliaires sur la base des facturations mensuelles. Elles sont ensuite affichées sur l’interface de visualisation.

### Opérations bancaires
__GET: [domain]:4000/v1/invoices/bank__  
La route /invoice/bank renvoie la liste de toutes les opérations bancaires effectuées.


## Diagramme fonctionnel 
![](echanges-Fonctionnel.png)
 
## Simulation sur 6 mois (01/06/2020 - 31/12/2020) 
 
>A la fin de chaque mois (le 25), l’équipe GAMERS transmet à notre SI une liste des factures du mois et leur date d’émission. Ensuite, au bout de quelques jours, lorsque la banque valide le paiement d’une facture, notre SI transmet à l’équipe GAMERS chaque facture payée. 
>Voici, sous forme de document JSON, les données transmises entre les SI de nos deux équipes chaques mois :
 
## 06/2020
### GAMERS &rightarrow; MONEY
```
[
    {
        "invoiceId": 03,
        "date": "2020-06-02",
        "clientId": 02,
        "amount": 7.5
    }
]
```
### MONEY &rightarrow; GAMERS
```
   {
     "invoiceId" : 03,
     "date" : "2020-06-27"
   }
```

## 07/2020 
### GAMERS &rightarrow; MONEY
```
[
    {
        "invoiceId": 04,
        "date": "2020-07-02",
        "clientId": 02,
        "amount": 15
    },
    {
        "invoiceId": 05,
        "date": "2020-07-17",
        "clientId": 03,
        "amount": 7.5
    }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 04,
       "date" : "2020-07-27"
   }
```
```
   {
       "invoiceId" : 05,
       "date" : "2020-07-30"
   }
```

## 08/2020:  
### GAMERS &rightarrow; MONEY
```
[
    {
        "invoiceId": 06,
        "date": "2020-08-02",
        "clientId": 02,
        "amount": 15
    },
    {
        "invoiceId": 07,
        "date": "2020-08-17",
        "clientId": 03,
        "amount": 15
    },
    {
        "invoiceId": 08,       
        "date": "2020-08-23",
        "clientId": 04,
        "amount": 7.5
    }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 06,
       "date" : "2020-08-27"
   }
```
```
   {
       "invoiceId" : 07,
       "date" : "2020-08-30"
   }
```
```
   {
       "invoiceId" : 08,
       "date" : "2020-08-28"
   }
```
## 09/2020:  
### GAMERS &rightarrow; MONEY
```
[
   {
       "invoiceId" : 09,
       "date" : "2020-09-05",
       "clientId": 02,
       "amount": 15
   },
   {
       "invoiceId" : 10,
       "date" : "2020-09-17",
       "clientId": 03,
       "amount": 15
   },
   {
       "invoiceId" : 11,
       "date" : "2020-09-23",
       "clientId": 04,
       "amount": 15
   },
   {
       "invoiceId" : 12,
       "date" : "2020-09-07",
       "clientId": 05,
       "amount": 7.5
   }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 09,
       "date" : "2020-09-27"
   }
```
```
   {
       "invoiceId" : 10,
       "date" : "2020-09-30"
   }
```
```
   {
       "invoiceId" : 11,
       "date" : "2020-09-28"
   }
```
```
   {
       "invoiceId" : 12,
       "date" : "2020-09-29"
   }
```

## 10/2020:  
### GAMERS &rightarrow; MONEY
```
[
   {
       "invoiceId" : 13,
       "date" : "2020-10-05",
       "clientId": 02,
       "amount": 15
   },
   {
       "invoiceId" : 14,
       "date" : "2020-10-17",
       "clientId": 03,
       "amount": 15
   },
   {
       "invoiceId" : 15,
       "date" : "2020-10-25",
       "clientId": 04,
       "amount": 15
   },
   {
       "invoiceId" : 16,
       "date" : "2020-10-29",
       "clientId": 05,
       "amount": 15
   },
   {
       "invoiceId" : 17,
       "date" : "2020-10-14",
       "clientId": 06,
       "amount": 7.5
   }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 13,
       "date" : "2020-10-27"
   }
```
```
   {
       "invoiceId" : 14,
       "date" : "2020-10-30"
   }
```
```
   {
       "invoiceId" : 15,
       "date" : "2020-10-28"
   }
```
```
   {
       "invoiceId" : 16,
       "date" : "2020-10-29"
   }
```
```
   {
       "invoiceId" : 17,
       "date" : "2020-10-27"
   }
```

## 11/2020:  
### GAMERS &rightarrow; MONEY
```
[
   {
       "invoiceId" : 18,
       "date" : "2020-11-05",
       "clientId": 02,
       "amount": 15
   },
   {
       "invoiceId" : 19,
       "date" : "2020-11-17",
       "clientId": 03,
       "amount": 15   
   },
   {
       "invoiceId" : 20,
       "date" : "2020-11-25",
       "clientId": 04,
       "amount": 15
   },
   {
       "invoiceId" : 21,
       "date" : "2020-11-29",
       "clientId": 05,
       "amount": 15
   },
   {
       "invoiceId" : 22,
       "date" : "2020-11-14",
       "clientId": 06,
       "amount": 15
   },
   {
       "invoiceId" : 23,
       "date" : "2020-11-22",
       "clientId": 07,
       "amount": 7.5
   }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 18,
       "date" : "2020-11-27"
   }
```
```
   {
       "invoiceId" : 19,
       "date" : "2020-11-30"
   }
```
```
   {
       "invoiceId" : 20,
       "date" : "2020-11-30"
   }
```
```
   {
       "invoiceId" : 21,
       "date" : "2020-11-28"
   }
```
```
   {
       "invoiceId" : 22,
       "date" : "2020-11-29"
   }
```
```
   {
       "invoiceId" : 23,
       "date" : "2020-11-29"
   }
```

## 12/2020
### GAMERS &rightarrow; MONEY
```
[
   {
       "invoiceId" : 24,
       "date" : "2020-12-05",
       "clientId": 02,
       "amount": 15
   },
   {
       "invoiceId" : 25,
       "date" : "2020-12-17",
       "clientId": 03,
       "amount": 15
   },
   {
       "invoiceId" : 26,
       "date" : "2020-12-25",
       "clientId": 04,
       "amount": 15
   },
   {
       "invoiceId" : 27,
       "date" : "2020-12-29",
       "clientId": 05,
       "amount": 15
   },
   {
       "invoiceId" : 28,
       "date" : "2020-12-14",
       "clientId": 06,
       "amount": 15
   },
   {
       "invoiceId" : 29,
       "date" : "2020-12-22",
       "clientId": 07,
       "amount": 15
   },
   {
       "invoiceId" : 30,
       "date" : "2020-12-30",
       "clientId": 08,
       "amount": 7.5
   }
]
```
### MONEY &rightarrow; GAMERS
```
   {
       "invoiceId" : 24,
       "date" : "2020-12-08"
   }
```
```
   {
       "invoiceId" : 25,
       "date" : "2020-12-28"
   }
```
```
   {
       "invoiceId" : 26,
       "date" : "2020-12-29"
   }
```
```
   {
       "invoiceId" : 27,
       "date" : "2020-12-29"
   }
```
```
   {
       "invoiceId" : 28,
       "date" : "2020-12-28"
   }
```
```
   {
       "invoiceId" : 29,
       "date" : "2020-12-30"
   }
```
```
   {
       "invoiceId" : 30,
       "date" : "2020-12-30"
   }
```
