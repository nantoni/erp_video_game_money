// Angular
import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { url, url2 } from 'src/app/views/projet/url';

@Component({
  selector: 'kt-widget9-recent-activities',
  templateUrl: './widget9-recent-activities.component.html'
})
export class Widget9RecentActivitiesComponent {
  @Input() cssClasses = '';
  
  listeCouleurs = ["text-warning",
  "text-success",
  "text-danger",
  "text-info",
  "text-primary"]

  dernieresActivites : DernieresActivites = new DernieresActivites();
  response: any;

  constructor(private http : HttpClient){}

  ngOnInit() {

    if(url2.length > 0)
    {    
      this.http.get<any>(url2 + "/v1/dashboard/activities").subscribe(
        value => 
        {
          this.response = value.dernieresActivites;

          this.dernieresActivites.nbVentes = value.nbVentes;

          this.response.data.forEach(element => {
            this.dernieresActivites.data.push({date: element.date, idClient : element.idClient, montantCommande: element.montantCommande, couleur: this.listeCouleurs[element.idClient%5]})
            }
          );
        });
    }
    else
    {

      this.dernieresActivites.nbVentes = "304 304";
      this.dernieresActivites.data = [{date: new Date("2020-01-23T00:00:00.000Z"), couleur: this.listeCouleurs[0], idClient : "1", montantCommande: '7'},
      {date: new Date("2020-01-11T00:00:00.000Z"), couleur: this.listeCouleurs[1], idClient : "2", montantCommande: '7'},
      {date: new Date("2020-01-10T00:00:00.000Z"), couleur: this.listeCouleurs[2], idClient : "3",  montantCommande: '8'},
      {date: new Date("2020-01-07T00:00:00.000Z"), couleur: this.listeCouleurs[3], idClient : "4",  montantCommande: '230'},
      {date: new Date("2020-01-05T00:00:00.000Z"), couleur: this.listeCouleurs[4], idClient : "5",  montantCommande: '5'}];
  
    }
  }

}

export class DernieresActivites
{
	nbVentes : string;
  data: Activites[] = [];
}

export class Activites
{
  date : Date;
  idClient : string;
  montantCommande : string;
  couleur : string;
}