// Angular
import { HttpClient } from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import { url, url2 } from 'src/app/views/projet/url';
import {LayoutConfigService} from '../../../../../core/_base/layout';

@Component({
  selector: 'kt-widget7-weekly-sales',
  templateUrl: './widget7-weekly-sales.component.html'
})
export class Widget7WeeklySalesComponent implements OnInit {
  @Input() cssClasses = '';
  chartOptions: any = {};
  fontFamily = '';
  colorsGrayGray500 = '';
  colorsGrayGray200 = '';
  colorsGrayGray300 = '';
  colorsThemeBaseDanger = '';
  colorsThemeBasePrimary = '';
  colorsThemeLightPrimary = '';
  colorsThemeBaseSuccess = '';
  colorsThemeLightSuccess = '';

  revenuMensuel : RevenuMensuel = new RevenuMensuel();

  constructor(private layoutConfigService: LayoutConfigService, private http: HttpClient) {
    this.fontFamily = this.layoutConfigService.getConfig('js.fontFamily');
    this.colorsGrayGray500 = this.layoutConfigService.getConfig('js.colors.gray.gray500');
    this.colorsGrayGray200 = this.layoutConfigService.getConfig('js.colors.gray.gray200');
    this.colorsGrayGray300 = this.layoutConfigService.getConfig('js.colors.gray.gray300');
    this.colorsThemeBaseDanger = this.layoutConfigService.getConfig('js.colors.theme.base.danger');
    this.colorsThemeBasePrimary = this.layoutConfigService.getConfig('js.colors.theme.base.primary');
    this.colorsThemeLightPrimary = this.layoutConfigService.getConfig('js.colors.theme.light.primary');
    this.colorsThemeBaseSuccess = this.layoutConfigService.getConfig('js.colors.theme.base.success');
    this.colorsThemeLightSuccess = this.layoutConfigService.getConfig('js.colors.theme.light.success');
  }

  ngOnInit(): void {

    if(url2.length > 0)
    {    
      this.http.get<any>(url2 + "/v1/dashboard/monthly").subscribe(
        
        value => {this.revenuMensuel = value.revenuMensuel;this.chartOptions = this.getChartOptions();
        }
      );
    }
    else
    {
      this.revenuMensuel = {
        revenu : 1320,
        revenuMois : [30, 45, 32, 70, 40,12,130,30]
      };
    }

    this.chartOptions = this.getChartOptions();


  }

  getChartOptions() {
    return {
      series: [
        {
          name: 'Chiffre d\'affaire',
          data: this.revenuMensuel.revenuMois
        }
      ],
      chart: {
        type: 'area',
        height: 150,
        toolbar: {
          show: false
        },
        zoom: {
          enabled: false
        },
        sparkline: {
          enabled: true
        }
      },
      plotOptions: {},
      legend: {
        show: false
      },
      dataLabels: {
        enabled: false
      },
      fill: {
        type: 'solid',
        opacity: 1
      },
      stroke: {
        curve: 'smooth',
        show: true,
        width: 3,
        colors: [this.colorsThemeBaseSuccess]
      },
      xaxis: {
        categories: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jui', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: false,
          style: {
            colors: this.colorsGrayGray500,
            fontSize: '12px',
            fontFamily: this.fontFamily
          }
        },
        crosshairs: {
          show: false,
          position: 'front',
          stroke: {
            color: this.colorsGrayGray300,
            width: 1,
            dashArray: 3
          }
        },
        tooltip: {
          enabled: true,
          formatter: undefined,
          offsetY: 0,
          style: {
            fontSize: '12px',
            fontFamily: this.fontFamily
          }
        }
      },
      yaxis: {
        labels: {
          show: false,
          style: {
            colors: this.colorsGrayGray500,
            fontSize: '12px',
            fontFamily: this.fontFamily
          }
        }
      },
      states: {
        normal: {
          filter: {
            type: 'none',
            value: 0
          }
        },
        hover: {
          filter: {
            type: 'none',
            value: 0
          }
        },
        active: {
          allowMultipleDataPointsSelection: false,
          filter: {
            type: 'none',
            value: 0
          }
        }
      },
      tooltip: {
        style: {
          fontSize: '12px',
          fontFamily: this.fontFamily
        },
        y: {
          // tslint:disable-next-line
          formatter: function (val) {
            return val + '€';
          }
        }
      },
      colors: [this.colorsThemeLightSuccess],
      markers: {
        colors: this.colorsThemeLightSuccess,
        strokeColor: [this.colorsThemeBaseSuccess],
        strokeWidth: 3
      }
    };
  }
}


export class RevenuMensuel {
  revenu : number;
  revenuMois : number[]
}
