// Angular
import { HttpClient } from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import { url, url2 } from 'src/app/views/projet/url';
import {LayoutConfigService} from '../../../../../core/_base/layout';

@Component({
  selector: 'kt-widget1-sales-stat',
  templateUrl: './widget1-sales-stat.component.html',
})
export class Widget1SalesStatComponent implements OnInit {
  @Input() cssClasses = '';
  chartOptions: any = {};
  fontFamily = '';
  colorsGrayGray500 = '';
  colorsGrayGray200 = '';
  colorsGrayGray300 = '';
  colorsThemeBaseDanger = '';
  stat: Statistiques = new Statistiques();

  constructor(private layoutConfigService: LayoutConfigService, private http: HttpClient) {
    this.fontFamily = this.layoutConfigService.getConfig('js.fontFamily');
    this.colorsGrayGray500 = this.layoutConfigService.getConfig('js.colors.gray.gray500');
    this.colorsGrayGray200 = this.layoutConfigService.getConfig('js.colors.gray.gray200');
    this.colorsGrayGray300 = this.layoutConfigService.getConfig('js.colors.gray.gray300');
    this.colorsThemeBaseDanger = this.layoutConfigService.getConfig('js.colors.theme.base.danger');
  }

  ngOnInit(): void {
    this.chartOptions = this.getChartOptions();

    if(url2.length > 0)
    {    
      this.http.get<any>(url2 + "/v1/dashboard/weekly").subscribe(
        value => 
        {
          this.stat = value.statistiquesSemaine;
        });
    }
    else
    {
      this.stat = {
        chiffreAffaire : 1320,
        nouveauxClients : 40,
        nbCommandes : 74,
        enAttente : 240
      };
    }

  }

  getChartOptions() {
    const strokeColor = '#D13647';
    return {
      series: [
        {
          name: 'Net Profit',
          data: [30, 45, 32, 70, 40, 40, 40]
        }
      ],
      chart: {
        type: 'area',
        height: 200,
        toolbar: {
          show: false
        },
        zoom: {
          enabled: false
        },
        sparkline: {
          enabled: true
        },
        dropShadow: {
          enabled: true,
          enabledOnSeries: undefined,
          top: 5,
          left: 0,
          blur: 3,
          color: strokeColor,
          opacity: 0.5
        }
      },
      plotOptions: {},
      legend: {
        show: false
      },
      dataLabels: {
        enabled: false
      },
      fill: {
        type: 'solid',
        opacity: 0
      },
      stroke: {
        curve: 'smooth',
        show: true,
        width: 3,
        colors: [strokeColor]
      },
      xaxis: {
        categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: false,
          style: {
            colors: this.colorsGrayGray500,
            fontSize: '12px',
            fontFamily: this.fontFamily
          }
        },
        crosshairs: {
          show: false,
          position: 'front',
          stroke: {
            color: this.colorsGrayGray300,
            width: 1,
            dashArray: 3
          }
        }
      },
      yaxis: {
        min: 0,
        max: 80,
        labels: {
          show: false,
          style: {
            colors: this.colorsGrayGray500,
            fontSize: '12px',
            fontFamily: this.fontFamily
          }
        }
      },
      states: {
        normal: {
          filter: {
            type: 'none',
            value: 0
          }
        },
        hover: {
          filter: {
            type: 'none',
            value: 0
          }
        },
        active: {
          allowMultipleDataPointsSelection: false,
          filter: {
            type: 'none',
            value: 0
          }
        }
      },
      tooltip: {
        enabled: false,
        style: {
          fontSize: '12px',
          fontFamily: this.fontFamily
        },
        y: {
          // tslint:disable-next-line
          formatter: function (val) {
            return '$' + val + ' thousands';
          }
        },
        marker: {
          show: false
        }
      },
      colors: ['transparent'],
      markers: {
        colors: this.colorsThemeBaseDanger,
        strokeColor: [strokeColor],
        strokeWidth: 3
      }
    };
  }
}


export class Statistiques{
  chiffreAffaire : any;
  nouveauxClients : any;
  nbCommandes : any;
  enAttente : any;
}