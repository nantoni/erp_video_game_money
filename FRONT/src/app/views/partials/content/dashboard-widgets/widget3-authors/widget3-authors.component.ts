// Angular
import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { url, url2 } from 'src/app/views/projet/url';

@Component({
  selector: 'kt-widget3-authors',
  templateUrl: './widget3-authors.component.html'
})
export class Widget3NewArrivalsAuthorsComponent {
  @Input() cssClasses = '';

  listeImg = ["./assets/media/svg/avatars/009-boy-4.svg",
  "./assets/media/svg/avatars/006-girl-3.svg",
  "./assets/media/svg/avatars/011-boy-5.svg",
  "./assets/media/svg/avatars/015-boy-6.svg",
  "./assets/media/svg/avatars/016-boy-7.svg"]

  topClients : TopClients[] = [];
  response: any;

  constructor(private http: HttpClient){}

  ngOnInit() {

    if(url2.length > 0)
    {    
      this.http.get<any>(url2 + "/v1/dashboard/top-clients").subscribe(
        value => 
        {
          this.response = value.topClients;
          this.response.forEach(element => {
            this.topClients.push({id: element.idClient, nbVentes: element.nbCommandes, img: this.listeImg[element.idClient%4]})
            }
          );
        });
    }
    else
    {
      this.topClients = [
        {id: 1, nbVentes: 5403, img: this.listeImg[0]},
        {id: 2, nbVentes: 340, img: this.listeImg[1]},
        {id: 3, nbVentes: 120, img: this.listeImg[2]},
        {id: 4, nbVentes: 57, img: this.listeImg[3]}
      ];
    }
  }
}

export class TopClients {
  id: number;
  nbVentes : number;
  img: string;
}