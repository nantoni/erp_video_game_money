import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { url, url2 } from '../url';
import { EcritureComptablesAuxiliairesModule } from './ecriture-comptables-auxiliaires.module';

@Component({
  selector: 'kt-ecriture-comptables-auxiliaires',
  templateUrl: './ecriture-comptables-auxiliaires.component.html',
  styleUrls: ['./ecriture-comptables-auxiliaires.component.scss']
})
export class EcritureComptablesAuxiliairesComponent implements OnInit {

  displayedColumns = ['date', 'client', 'ref', 'debit', 'credit'];
  dataSource = new MatTableDataSource();
  response: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    if(url2.length > 0)
    {    
      this.http.get<EcritureComptablesAuxiliaires[]>(url2 + "/v1/invoices/auxiliar").subscribe(
        value => 
        {
          this.response = value;
          this.response.forEach(element => {
            this.dataSource.data.push({ date: element.date, client: element.clientId, ref: "Facture " + element.invoiceId, debit: element.debit, credit: element.credit })
            this.dataSource._updateChangeSubscription();            
          }
          );
        }
      );
    }
    else
    {
      this.dataSource.data = [
  
        {date: "30/06/20", client: "Tintin", ref: "Facture 1", debit: "", credit: "6,00 €"},
        {date: "30/06/20", client: "Tonton", ref: "Facture 45", debit: "", credit: "1,50 €"},
        {date: "30/06/20", client: "Clopin", ref: "Facture 1" , debit: "7,50€", credit: ""},
        {date: "30/06/20", client: "Dardin", ref: "Facture 2", debit:"", credit: "6,00 €"},
        {date: "30/06/20", client: "Dupin", ref: "Facture 2", debit:"", credit: "1,50 €"}
      ];
    }
  }
}

export interface EcritureComptablesAuxiliaires {
  date: string;
  client: string;
  ref: string;
  debit: string;
  credit: string;
}