import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';
import { RouterModule } from '@angular/router';
import { EcritureComptablesAuxiliairesComponent } from './ecriture-comptables-auxiliaires.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    CdkTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: EcritureComptablesAuxiliairesComponent
      }, 
    ]),
    
  ]
})
export class EcritureComptablesAuxiliairesModule { }
