import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkTableModule } from '@angular/cdk/table';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { OperationsBancairesComponent } from './operations-bancaires.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    CdkTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: OperationsBancairesComponent
      }, 
    ]),
    
  ]
})
export class OperationsBancairesModule { }
