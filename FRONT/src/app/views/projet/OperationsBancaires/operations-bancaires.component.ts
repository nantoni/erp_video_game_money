import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { url, url2 } from '../url';

@Component({
  selector: 'kt-operations-bancaires',
  templateUrl: './operations-bancaires.component.html',
  styleUrls: ['./operations-bancaires.component.scss']
})
export class OperationsBancairesComponent implements OnInit {

  displayedColumns = ['date', 'ref', 'description', 'montant'];
  dataSource : OperationsBancaires[] = [];
  
  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    if(url2.length > 0)
    {    
      this.http.get<any>(url2 + "/v1/invoices/bank").subscribe(
        value => this.dataSource = value
      );
    }
    else
    {
      this.dataSource = [
  
        {date: "30/06/20", ref: 10, description: "Prelevement Tintin",  montant: 6},
        {date: "30/06/20", ref: 13, description: "Prelevement Tonton" , montant: 1},
        {date: "30/06/20", ref: 234, description: "Prelevement Clopin" , montant: 7},
        {date: "30/06/20", ref: 405, description: "Prelevement Dardin", montant: 6},
        {date: "30/06/20", ref: 50, description: "Prelevement Dupin", montant: 1}
      ];
    }

  }

}

export class OperationsBancaires {
  date: string;
  ref: number;
  description: string;
  montant: number;
}
