import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EcritureComptablesGeneralesComponent } from './ecriture-comptables-generales.component';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';




@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    CdkTableModule,
    RouterModule.forChild([
      {
        path: '',
        component: EcritureComptablesGeneralesComponent
      }, 
    ]),
    
  ]
})
export class EcritureComptablesGeneralesModule { }
