import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { url2 } from '../url';

@Component({
  selector: 'kt-ecriture-comptables-generales',
  templateUrl: './ecriture-comptables-generales.component.html',
  styleUrls: ['./ecriture-comptables-generales.component.scss']
})

export class EcritureComptablesGeneralesComponent implements OnInit {

  displayedColumns = ['date', 'compte', 'description', 'ref', 'debit', 'credit'];
  dataSource = new MatTableDataSource<EcritureComptablesGenerales>();//EcritureComptablesGenerales[] = [];
  response: any;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {


    if(url2.length > 0)
    {    
      this.http.get<EcritureComptablesGenerales[]>(url2 + "/v1/invoices").subscribe(
        value => 
        {
          this.response = value;
          this.response.forEach(element => {
            this.dataSource.data.push({ date: element.date, compte: element.account, description: element.description, ref: "Facture " + element.invoiceId, debit: element.debit, credit: element.credit })
            this.dataSource._updateChangeSubscription();
            
          }
          );
        });
    }
    else
    {
      this.dataSource.data = [
  
        {date: "30/06/20", compte: "700", description: "Produit", ref: "Facture1", debit: "", credit: "6,00 €"},
        {date: "30/06/20", compte: "445", description: "TVA", ref: "Facture1", debit: "", credit: "1,50 €"},
        {date: "30/06/20", compte: "411", description: "Client", ref: "Facture 1" , debit: "7,50€", credit: ""},
        {date: "30/06/20", compte: "700", description: "Produit", ref: "Facture2", debit:"", credit: "6,00 €"},
        {date: "30/06/20", compte: "445", description: "TVA", ref: "Facture2", debit:"", credit: "1,50 €"}
      ];
    }

  }

}

export interface EcritureComptablesGenerales {
  date: string;
  compte: string;
  description: string;
  ref: string;
  debit: string;
  credit: string;
}