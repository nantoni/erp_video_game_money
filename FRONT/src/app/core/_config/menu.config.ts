export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
        {
          title: 'Accueil',
          root: true,
          alignment: 'left',
          page: '/accueil',
        }
      ]
    },
    aside: {
      self: {},
      items: [
        {
          title: 'Accueil',
          root: true,
          icon: 'flaticon2-architecture-and-city',
          page: '/accueil',
          bullet: 'dot',
        },
        {section: 'Administration'},
        {
          title: 'Comptabilité Générale',
          root: true,
          icon: 'flaticon-price-tag',
          page: '/ecriture-comptables-generales',
          bullet: 'dot',
        },
        {
          title: 'Comptabilité Auxiliaire',
          root: true,
          icon: 'flaticon-arrows',
          page: '/ecriture-comptables-auxiliaires',
          bullet: 'dot',
        },
        {
          title: 'Operations Bancaires',
          root: true,
          icon: 'flaticon-piggy-bank',
          page: '/operations-bancaires',
          bullet: 'dot',
        }
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
