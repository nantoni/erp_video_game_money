const path =
  process.env.NODE_ENV === "test"
    ? `${__dirname}/.env.test`
    : `${__dirname}/.env`;
require("dotenv").config({ path });

module.exports = {
  local: {
    url: process.env.DATABASE_URL,
    dialect: "mysql"
  },
  test: {
    url: process.env.DATABASE_URL,
    dialect: "mysql"
  }
};
