import {
  Options,
  TimeoutError,
  ConnectionError,
  ConnectionRefusedError,
  HostNotReachableError,
  ConnectionTimedOutError
} from "sequelize";

import ENV from "../env";

const SEQUELIZE: Options = {
  dialect: "mysql",
  timezone: "+00:00",
  pool: {
    min: 0,
    max: ENV.DATABASE_MAX_CONNECTION,
    acquire: 30000,
    idle: 10000
  },
  query: {
    raw: false
  },
  retry: {
    match: [
      TimeoutError,
      ConnectionError,
      ConnectionRefusedError,
      HostNotReachableError,
      ConnectionTimedOutError
    ],
    max: 1
  },
  define: {
    underscored: true,
    freezeTableName: true,
    charset: "utf8",
    collate: "utf8_general_ci",
    timestamps: false
  }
};

const DATABASE_CONFIG = Object.freeze({
  SEQUELIZE
});

export default DATABASE_CONFIG;
