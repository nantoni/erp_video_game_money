const INVOICE = Object.freeze({
  DESCIPTION: {
    MAX_LENGTH: 100
  },
  ACCOUNT: {
    PRODUCT: {
      ID: 700,
      DESCIPTION: "Produit"
    },
    VAT: {
      ID: 445,
      DESCIPTION: "VAT"
    },
    CLIENT: {
      ID: 411,
      DESCIPTION: "Client"
    },
    BANK: {
      ID: 511,
      DESCIPTION: "Bank"
    }
  }
});

export { INVOICE };
