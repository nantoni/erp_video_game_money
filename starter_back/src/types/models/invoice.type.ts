interface InvoiceCreate {
  invoiceId: number;
  clientId: number;
  date: Date;
  amount: number;
  VAT?: number;
}

interface InvoiceRawCreate {
  invoiceId: number;
  date: Date;
  account: number;
  description: string;
  debit: number | null;
  credit: number | null;
}

export { InvoiceCreate, InvoiceRawCreate };
