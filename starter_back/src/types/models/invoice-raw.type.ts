interface AttributesType {
  invoiceId: number;
  account: number;
  description: string;
  debit: number | null;
  credit: number | null;
  date: Date;
}

export { AttributesType };
