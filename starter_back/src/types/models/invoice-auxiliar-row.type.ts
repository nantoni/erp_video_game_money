interface AttributesType {
  invoiceId: number;
  clientId: number;
  debit: number | null;
  credit: number | null;
  date: Date;
}

interface InvoiceAuxiliarRawCreate {
  invoiceId: number;
  clientId: number;
  date: Date;
  debit: number | null;
  credit: number | null;
}

export { AttributesType, InvoiceAuxiliarRawCreate };
