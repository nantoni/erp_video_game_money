interface WeeklyFigures {
  chiffreAffaire: number;
  nouveauxClients: number;
  nbCommandes: number;
  enAttente: number;
}

interface MonthlyFigures {
  revenu: number;
  revenuMois: number[];
}

interface NewClientsFigures {
  nbNouveauxClients: number;
  nbNouveauxClientsMois: number[];
}

interface Client {
  idClient: number;
  nbCommandes: number;
}

interface lastActivities {
  nbVentes: number;
  data: lastActivitiesData[];
}
interface lastActivitiesData {
  date: Date;
  idClient: number;
  montantCommande: number;
}

export {
  WeeklyFigures,
  MonthlyFigures,
  NewClientsFigures,
  Client,
  lastActivities
};
