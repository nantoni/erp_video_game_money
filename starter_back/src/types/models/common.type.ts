import { DataType, ModelAttributeColumnOptions } from "sequelize";

type ModelInitType<T> = {
  [property in keyof Required<T>]: DataType | ModelAttributeColumnOptions;
};

export { ModelInitType };
