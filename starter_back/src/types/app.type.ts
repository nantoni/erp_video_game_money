import { Router } from "express";

import { BaseModel } from "../models";

interface RouteType {
  path: string;
  router: Router;
}

export { RouteType };
