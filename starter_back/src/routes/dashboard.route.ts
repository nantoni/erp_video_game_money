import { Router, Request, Response } from "express";
import HttpStatus from "http-status-codes";
import asyncHandler from "express-async-handler";

import { ERROR, INVOICE } from "../constants";

import ClientError from "../client-error";
import { InvoiceRaw, InvoiceAuxiliarRow } from "../models";
import {
  InvoiceService,
  InvoiceAuxiliarRowService,
  DashboardService
} from "../services";
import { InvoiceValidation } from "../validations";
import { Op } from "sequelize";

const router: Router = Router();

/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get weekly incomes figures"
 *    description: "Get weekly incomes"
 *    operationId: "GetWeeklyIncomes"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/WeeklyIncomes'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/WeeklyIncomes'
 */
router.get(
  "/weekly",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    res.status(HttpStatus.OK).json({
      statistiquesSemaine: await DashboardService.getWeeklyIncomes()
    });
  })
);

/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get monthly incomes figures"
 *    description: "Get weekly incomes"
 *    operationId: "GetWeeklyIncomes"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/MonthlyIncomes'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/MonthlyIncomes'
 */
router.get(
  "/monthly",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    res.status(HttpStatus.OK).json({
      revenuMensuel: await DashboardService.getMonthlyIncomes()
    });
  })
);

/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get list of new client for last month"
 *    description: "Get new clients"
 *    operationId: "GetNewClients"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Clients'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Clients'
 */
router.get(
  "/new-clients",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    res.status(HttpStatus.OK).json({
      revenuMensuel: await DashboardService.getNewClientsFigures()
    });
  })
);

/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get top clients for last month"
 *    description: "Get top clients"
 *    operationId: "GetTopClients"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Client'
 */
router.get(
  "/top-clients",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    res.status(HttpStatus.OK).json({
      topClients: await DashboardService.getTopClientsFigures()
    });
  })
);

/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get last activities registered in database"
 *    description: "Get activities"
 *    operationId: "GetActivities"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Activities'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Activities'
 */
router.get(
  "/activities",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    res.status(HttpStatus.OK).json({
      dernieresActivites: await DashboardService.getLastActivities()
    });
  })
);

export default router;
