import InvoiceRoute from "./invoice.route";
import DashboardRoute from "./dashboard.route";

import { RouteType } from "../types/app.type";

const routes: RouteType[] = [
  {
    path: "invoices",
    router: InvoiceRoute
  },
  {
    path: "dashboard",
    router: DashboardRoute
  }
];

export default routes;
