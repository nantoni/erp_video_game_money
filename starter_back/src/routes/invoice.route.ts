import { Router, Request, Response } from "express";
import HttpStatus from "http-status-codes";
import asyncHandler from "express-async-handler";

import { validationMiddleware } from "../middlewares";

import { ERROR, INVOICE } from "../constants";

import ClientError from "../client-error";
import { InvoiceRaw, InvoiceAuxiliarRow } from "../models";
import { InvoiceService, InvoiceAuxiliarRowService } from "../services";
import { InvoiceValidation } from "../validations";

const router: Router = Router();

/**
 * @swagger
 * /invoices:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get all invoice rows"
 *    description: "Get invoices"
 *    operationId: "GetInvoices"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Invoice'
 */
router.get(
  "/",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    const invoice: InvoiceRaw[] | null = await InvoiceService.findAllRaws({
      limit: 100
    });

    res.status(HttpStatus.OK).json(invoice);
  })
);

/**
 * @swagger
 * /invoices/auxiliar:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get invoice auxiliar rows"
 *    description: "Get invoice auxiliar rows"
 *    operationId: "GetInvoiceAuxiliar"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/InvoiceRow'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/InvoiceRow'
 */
router.get(
  "/auxiliar",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    const invoiceRows:
      | InvoiceAuxiliarRow[]
      | null = await InvoiceAuxiliarRowService.findAllRaws({ limit: 100 });

    res.status(HttpStatus.OK).json(invoiceRows);
  })
);

/**
 * @swagger
 * /invoices:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get all bank rows"
 *    description: "Get bank rows"
 *    operationId: "GetBankRows"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankRows'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/BankRows'
 */
router.get(
  "/bank",
  [],
  asyncHandler(async (req: Request, res: Response) => {
    const bankTransactions = await InvoiceAuxiliarRowService.getBankTransactions();

    res.status(HttpStatus.OK).json(bankTransactions);
  })
);

/**
 * @swagger
 * /invoices/{invoiceId}:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get an invoice from invoiceId"
 *    description: "Get invoice"
 *    operationId: "GetInvoice"
 *    parameters:
 *      - $ref: '#/components/parameters/InvoiceId'
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Invoice'
 *      404:
 *        description: "Invoice not found"
 */
router.get(
  "/:invoiceId",
  [validationMiddleware(InvoiceValidation.invoiceIdParam)],
  asyncHandler(async (req: Request, res: Response) => {
    const invoiceId: number = parseInt(req.params.invoiceId, 10);
    const invoice:
      | InvoiceRaw[]
      | null = await InvoiceService.findAllByInvoiceId(invoiceId);

    if (invoice === null) {
      throw new ClientError({
        status: HttpStatus.NOT_FOUND,
        name: ERROR.NAME.INVOICE,
        message: ERROR.INVOICE.NOT_FOUND,
        originalError: new Error("invoice.route.ts > GET invoices{invoiceId}")
      });
    }

    res.status(HttpStatus.OK).json(invoice);
  })
);

/**
 * @swagger
 * /invoices:
 *  post:
 *    tags:
 *      - "Invoices"
 *    summary: "add invoices"
 *    description: "Add new invoices from body"
 *    operationId: "PostInvoices"
 *    requestBody:
 *      $ref: '#/components/requestBodies/CreateInvoice'
 *    responses:
 *      200:
 *        description: "created"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/InvoiceDefault'
 *            examples:
 *              User:
 *                $ref: '#/components/examples/Invoice'
 *      400:
 *        description: "JOI Validation Error, wrong param and/or body passed"
 */
router.post(
  "/",
  [validationMiddleware(InvoiceValidation.create)],
  asyncHandler(async (req: Request, res: Response) => {
    for (const invoiceBody of req.body) {
      await InvoiceService.createInvoice(invoiceBody);
    }
    res.status(HttpStatus.OK).json();
  })
);

export default router;
