import swaggerJSDoc from "swagger-jsdoc";
import * as fs from "fs";
import * as path from "path";

import Logger from "./helpers/logger.helper";

import baseConfig from "./swagger-base.config.json";

const INDENT = 2;

const build = (): swaggerJSDoc.Options => {
  const swagger: object = swaggerJSDoc({
    ...baseConfig,
    apis: [
      `${__dirname}/constants/*.constant.*`,
      `${__dirname}/models/*.model.*`,
      `${__dirname}/routes/*.route.*`,
      `${__dirname}/validations/*.validation.*`,
      `${__dirname}/schemes/*.scheme.*`
    ]
  });

  try {
    const file: string = JSON.stringify(swagger, null, INDENT);
    fs.writeFileSync(path.resolve(__dirname, "openapi.json"), file, "utf8");
  } catch (err) {
    Logger.log(err);
  }

  return swagger;
};

export default build;
