import cron from "cron";
import axios from "axios";

import InvoiceAuxiliarRowService from "./services/invoice-auxiliar-row.service";
import logger from "./helpers/logger.helper";
import { initModels } from "./models";
import { Op } from "sequelize";

// destroy all old consumer logbook infos, depending days count limit provided in .ENV
new cron.CronJob({
  cronTime: "0 */5 * * * *",
  onTick: async function (): Promise<void> {
    try {
      initModels();
      const bankTransactions = await InvoiceAuxiliarRowService.findAllRaws({
        where: {
          debit: { [Op.ne]: null }
        }
      });

      const bankTransactionsIds = bankTransactions.map((transaction) => {
        return transaction.invoiceId;
      });

      const clientTransactions = await InvoiceAuxiliarRowService.findAllRaws({
        where: {
          credit: { [Op.ne]: null },
          invoiceId: { [Op.notIn]: bankTransactionsIds }
        }
      });

      const createdBankTransactions = [];

      for (const clientTransaction of clientTransactions) {
        const bankTransaction = await InvoiceAuxiliarRowService.createInvoiceAuxiliarRow(
          {
            invoiceId: clientTransaction.invoiceId,
            clientId: clientTransaction.clientId,
            date: new Date(),
            debit: clientTransaction.credit,
            credit: null
          }
        );

        createdBankTransactions.push({
          invoiceId: bankTransaction.invoiceId,
          date: bankTransaction.date
        });
      }

      if (createdBankTransactions.length > 0) {
        logger.debug(
          "CRON - POST validations to Video Games group - DATE : ",
          new Date()
        );
        await axios.post(
          "http://cnamerp.leojullerot.fr:4689/setPayments",
          createdBankTransactions
        );
      }

      logger.debug(
        "CRON - Bank has validated transactions - DATE : ",
        new Date()
      );
    } catch (err) {
      logger.error("CRON - ERROR in Bank valiadations : ", err);
    }
  },
  start: true,
  timeZone: "Europe/Paris"
});
