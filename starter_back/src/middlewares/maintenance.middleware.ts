import { NextFunction, Request, Response } from "express";
import HttpStatus from "http-status-codes";

import ClientError from "../client-error";

import { ERROR, GLOBAL } from "../constants";
import ENV from "../env";

export default () => {
  return (req: Request, res: Response, next: NextFunction): void => {
    if (ENV.IS_UNDER_MAINTENANCE) {
      next(
        new ClientError({
          name: ERROR.NAME.MAINTENANCE,
          level: GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
          status: HttpStatus.SERVICE_UNAVAILABLE,
          message: ERROR.MAINTENANCE.IS_ON
        })
      );
    } else {
      next();
    }
  };
};
