import { ObjectSchema, ValidationOptions } from "@hapi/joi";
import HttpStatus from "http-status-codes";
import { NextFunction, Request, Response } from "express";

import ClientError from "../client-error";

import { ERROR, GLOBAL } from "../constants";

const JoiValidationOptions: ValidationOptions = {
  abortEarly: false,
  allowUnknown: false,
  stripUnknown: false
};

export default (
  schema: ObjectSchema
): ((req: Request, res: Response, next: NextFunction) => void) => {
  return (req: Request, res: Response, next: NextFunction): void => {
    const toValidate = {
      ...(Object.entries(req.params).length > 0 && { params: req.params }),
      ...(req.body &&
        Object.entries(req.body).length > 0 && { body: req.body }),
      ...(Object.entries(req.query).length > 0 && { query: req.query })
    };
    const { error: validationError } = schema.validate(
      toValidate,
      JoiValidationOptions
    );

    const valid: boolean = validationError == null;
    if (valid) {
      next();
    } else {
      const { details } = validationError;
      next(
        new ClientError({
          name: ERROR.NAME.VALIDATION,
          level: GLOBAL.LOGGER.LOGGER_LEVEL.WARN,
          message: ERROR.JOI_VALIDATION,
          status: HttpStatus.BAD_REQUEST,
          details,
          originalError: validationError
        })
      );
    }
  };
};
