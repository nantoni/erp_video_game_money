import HttpStatus from "http-status-codes";
import { NextFunction, Request, Response } from "express";
import { ValidationError } from "sequelize";

import ClientError, { ClientErrorType } from "../client-error";
import Logger from "../helpers/logger.helper";

import { GLOBAL } from "../constants";

const sequelizeErrorHandler = (
  err: Error | ValidationError | ClientErrorType,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  let clientError: ClientError;
  if (err instanceof ValidationError) {
    clientError = new ClientError({
      name: err.name,
      message: err.message,
      level: GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      details: err.errors,
      originalError: err
    });
  }
  if (err.constructor.name === "EagerLoadingError") {
    clientError = new ClientError({
      name: err.name,
      message: err.message,
      level: GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      originalError: err
    });
    next(clientError);
  }
  next(err);
};

const otherErrorHandler = (
  err: Error | ClientErrorType,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  if (!(err instanceof ClientError) && err instanceof Error) {
    next(
      new ClientError({
        name: err.name,
        message: err.message,
        level: GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        originalError: err
      })
    );
  } else if (!(err instanceof ClientError)) {
    console.log(
      "____add error handler____",
      err instanceof Error,
      err.constructor.name,
      err
    );
  }
  next(err);
};

const logErrors = (
  err: ClientError,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  Logger.log(err.level, err.getLogError());
  next(err);
};

const errorHandler = (
  err: ClientError,
  req: Request,
  res: Response,
  _next: NextFunction
): void => {
  res.status(err.status).send({ error: err.getError() });
};

export default {
  sequelizeErrorHandler,
  otherErrorHandler,
  logErrors,
  errorHandler
};
