import http, { Server } from "http";
import express, { Application, Request, Response, Router } from "express";
import cors from "cors";
import bodyParser from "body-parser";
import compression from "compression";
import helmet from "helmet";
import morgan from "morgan";
import asyncHandler from "express-async-handler";
import rateLimit from "express-rate-limit";
import HttpStatus from "http-status-codes";

import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

import ENV from "./env";
import ClientError from "./client-error";
import { ERROR, GLOBAL } from "./constants";
import SwaggerBuilder from "./swagger";

import { errorMiddleware, maintenanceMiddleware } from "./middlewares";
import Logger from "./helpers/logger.helper";
import Route from "./routes";
import { RouteType } from "./types/app.type";

class App {
  private readonly app: Application;
  private readonly router: Router;
  private readonly port: number;
  private server: Server;

  constructor(port: number) {
    this.app = express();
    this.router = Router();
    this.port = port;

    this.initSwagger();
    this.initMiddlewares();
    this.initRateLimiterMiddleware();
    this.initRoutes();
    this.initErrorMiddleware();
  }

  public get instance(): Application {
    return this.app;
  }

  public listen(): void {
    this.server = http.createServer(this.app);
    this.server.listen(this.port);
    console.log(`App listening on port ${this.port}`);
  }

  public close(): void {
    this.server.close();
  }

  private initSwagger(): void {
    if (ENV.NODE_ENV && ENV.NODE_ENV === "local") {
      const swaggerBuild: swaggerJSDoc.Options = SwaggerBuilder();
      this.app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerBuild));
      this.router.get("/api-docs", swaggerUi.setup(swaggerBuild));
    }
  }

  private initMiddlewares(): void {
    this.app
      .use(cors())
      .use(helmet())
      .use(bodyParser.urlencoded({ limit: "15mb", extended: false }))
      .use(bodyParser.json({ limit: "15mb" }))
      .use(compression())
      .use(
        morgan(
          ":method :url :status :remote-addr :response-time ms - :res[content-length]",
          {
            stream: {
              write: (message: string): void => {
                Logger.info(message);
              }
            }
          }
        )
      )
      .use(maintenanceMiddleware());
  }

  private initRateLimiterMiddleware(): void {
    if (ENV.NODE_ENV !== GLOBAL.ENV.TEST) {
      const windowMs: number = parseInt(ENV.RATE_LIMITER_WINDOW_MS, 10);
      const max: number = parseInt(ENV.RATE_LIMITER_MAX_COUNT, 10);

      const limiter = rateLimit({
        windowMs,
        max
      });

      this.app.use(limiter);
    }
  }

  private initErrorMiddleware(): void {
    this.app
      .use(errorMiddleware.sequelizeErrorHandler)
      .use(errorMiddleware.otherErrorHandler)
      .use(errorMiddleware.logErrors)
      .use(errorMiddleware.errorHandler);
  }

  private initRoutes(): void {
    Route.map((route: RouteType) => {
      const path: string = route.path;
      this.router.use(`/${path}`, route.router);
    });

    this.app.use("/v1", this.router);

    // 404 handler (ALWAYS keep this as the last route)
    this.app.all(
      "*",
      asyncHandler((req: Request, res: Response) => {
        const originalUrl: string = req.originalUrl;
        const method: string = req.method;

        const error = new ClientError({
          name: ERROR.NAME.ROUTER,
          message: `Route ${originalUrl} not found with ${method} method`,
          status: HttpStatus.NOT_FOUND
        });

        res.status(HttpStatus.NOT_FOUND).json({ error: error.getError() });
      })
    );
  }
}
export default App;
