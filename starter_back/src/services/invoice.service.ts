import { FindOptions, Order, WhereOptions, CreateOptions } from "sequelize";

import BaseService from "./base.service";
import { sequelize, InvoiceRaw } from "../models";
import ClientError from "../client-error";
import HttpStatus from "http-status-codes";
import { ERROR, INVOICE } from "../constants";
import { InvoiceCreate, InvoiceRawCreate } from "../types/models/invoice.type";
import InvoiceAuxiliarRowService from "./invoice-auxiliar-row.service";

class InvoiceService extends BaseService<InvoiceRaw> {
  public static async createInvoice(
    invoiceBody: InvoiceCreate
  ): Promise<InvoiceRaw[]> {
    const countInvoices: number = await this.count({
      where: { invoiceId: invoiceBody.invoiceId }
    });
    if (countInvoices > 0) {
      throw new ClientError({
        name: ERROR.NAME.VALIDATION,
        message: ERROR.INVOICE.ALREADY_EXISTS,
        status: HttpStatus.CONFLICT
      });
    }

    const vat = invoiceBody.VAT !== undefined ? invoiceBody.VAT : 20;

    const invoiceRaws = [
      {
        date: invoiceBody.date,
        account: INVOICE.ACCOUNT.VAT.ID,
        description: INVOICE.ACCOUNT.VAT.DESCIPTION,
        invoiceId: invoiceBody.invoiceId,
        debit: null,
        credit: (invoiceBody.amount * vat) / 100
      },
      {
        date: invoiceBody.date,
        account: INVOICE.ACCOUNT.PRODUCT.ID,
        description: INVOICE.ACCOUNT.PRODUCT.DESCIPTION,
        invoiceId: invoiceBody.invoiceId,
        debit: null,
        credit: invoiceBody.amount - (invoiceBody.amount * vat) / 100
      },
      {
        date: invoiceBody.date,
        account: INVOICE.ACCOUNT.PRODUCT.ID,
        description: INVOICE.ACCOUNT.PRODUCT.DESCIPTION,
        invoiceId: invoiceBody.invoiceId,
        debit: invoiceBody.amount,
        credit: null
      }
    ];

    const auxiliarRaw = {
      invoiceId: invoiceBody.invoiceId,
      clientId: invoiceBody.clientId,
      date: invoiceBody.date,
      debit: null,
      credit: invoiceBody.amount
    };

    await InvoiceAuxiliarRowService.createInvoiceAuxiliarRow(auxiliarRaw);
    return await this.createMany(invoiceRaws);
  }

  public static async findOneById(id: number): Promise<InvoiceRaw | null> {
    return await this.findOne({
      invoiceId: id
    });
  }

  public static async findAllRaws(
    options?: FindOptions
  ): Promise<InvoiceRaw[] | null> {
    return await this.findAll(options);
  }

  public static async countAllRaws(options: FindOptions): Promise<number> {
    return await this.count(options);
  }

  public static async findAllByInvoiceId(
    id: number
  ): Promise<InvoiceRaw[] | null> {
    return await this.findAll({
      where: { invoiceId: id }
    });
  }

  private static async findOne(
    optionsWhere: WhereOptions,
    optionsOrder: Order = [["date", "DESC"]]
  ): Promise<InvoiceRaw | null> {
    return InvoiceRaw.findOne({
      where: { ...optionsWhere },
      order: optionsOrder
    });
  }

  private static async findAll(
    options: FindOptions,
    optionsOrder: Order = [["date", "DESC"]]
  ): Promise<InvoiceRaw[] | null> {
    return InvoiceRaw.findAll({
      ...options,
      order: optionsOrder
    });
  }

  private static async create(
    invoiceValues: InvoiceCreate,
    options?: CreateOptions
  ): Promise<InvoiceRaw> {
    return InvoiceRaw.create(invoiceValues, options);
  }

  private static async createMany(
    invoiceValues: InvoiceRawCreate[],
    options?: CreateOptions
  ): Promise<InvoiceRaw[]> {
    return InvoiceRaw.bulkCreate(invoiceValues, options);
  }

  private static async count(options: FindOptions): Promise<number> {
    return InvoiceRaw.count(options);
  }
}
export default InvoiceService;
