import Sequelize, { Order, WhereOptions, CreateOptions, Op } from "sequelize";
import { ERROR, INVOICE } from "../constants";
import { format, addDays, subDays, subMonths } from "date-fns";

import BaseService from "./base.service";
import { InvoiceAuxiliarRow, InvoiceRaw, sequelize } from "../models";
import InvoiceService from "./invoice.service";
import InvoiceAuxiliarRowService from "./invoice-auxiliar-row.service";

import {
  WeeklyFigures,
  MonthlyFigures,
  NewClientsFigures,
  Client
} from "../types/models/dashboard.type";

class DashboardService extends BaseService<void> {
  public static async getWeeklyIncomes(): Promise<WeeklyFigures> {
    const weeklyIncomes = await InvoiceService.findAllRaws({
      where: {
        account: INVOICE.ACCOUNT.PRODUCT.ID,
        debit: { [Op.ne]: null },
        date: {
          [Op.between]: [
            format(subDays(new Date(), 7), "yyyy-MM-dd"),
            format(new Date(), "yyyy-MM-dd")
          ]
        }
      },
      attributes: ["debit"]
    });

    const oldClients = await InvoiceAuxiliarRowService.findAllRaws({
      where: {
        date: {
          [Op.lt]: [format(subDays(new Date(), 7), "yyyy-MM-dd")]
        }
      },
      attributes: ["clientId"]
    });

    const oldClientIds: number[] = oldClients.map((row) => {
      return row.clientId;
    });

    const newWeeklyClient = await InvoiceAuxiliarRowService.findAllRaws({
      where: {
        clientId: {
          [Op.notIn]: oldClientIds
        }
      }
    });

    const newClients: number[] = newWeeklyClient.map((row) => {
      return row.clientId;
    });

    const resClient = [];
    for (const newClient of newClients) {
      if (!resClient.includes(newClient)) {
        resClient.push(newClient);
      }
    }

    const weeklyOrdersCount = await InvoiceAuxiliarRowService.countAllRaws({
      where: {
        date: {
          [Op.between]: [
            format(subDays(new Date(), 7), "yyyy-MM-dd"),
            format(addDays(new Date(), 1), "yyyy-MM-dd")
          ]
        },
        credit: { [Op.ne]: null }
      }
    });

    const weeklyUnresolvedOrdersCount = await InvoiceAuxiliarRowService.findAllRaws(
      {
        where: {
          date: {
            [Op.between]: [
              format(subDays(new Date(), 7), "yyyy-MM-dd"),
              format(addDays(new Date(), 1), "yyyy-MM-dd")
            ]
          },
          credit: { [Op.ne]: null }
        }
      }
    );

    const weeklyResolvedOrdersCount = await InvoiceAuxiliarRowService.findAllRaws(
      {
        where: {
          date: {
            [Op.between]: [
              format(subDays(new Date(), 7), "yyyy-MM-dd"),
              format(addDays(new Date(), 1), "yyyy-MM-dd")
            ]
          },
          debit: { [Op.ne]: null }
        }
      }
    );

    const unresolved = weeklyUnresolvedOrdersCount.reduce(
      (a, b) => a + b.credit,
      0
    );
    const resolved = weeklyResolvedOrdersCount.reduce((a, b) => a + b.debit, 0);

    return {
      chiffreAffaire:
        weeklyIncomes !== null
          ? weeklyIncomes.reduce((a, b) => a + b.debit, 0)
          : 0,
      nouveauxClients: resClient.length,
      nbCommandes: weeklyOrdersCount,
      enAttente: unresolved - resolved
    };
  }

  public static async getMonthlyIncomes(): Promise<MonthlyFigures> {
    const monthlyIncomes = await InvoiceService.findAllRaws({
      where: {
        account: INVOICE.ACCOUNT.PRODUCT.ID,
        debit: { [Op.ne]: null },
        date: {
          [Op.between]: [
            format(subDays(new Date(), 31), "yyyy-MM-dd"),
            format(addDays(new Date(), 1), "yyyy-MM-dd")
          ]
        }
      },
      attributes: ["debit"]
    });

    const yearlyIncomes = await InvoiceService.findAllRaws({
      where: {
        account: INVOICE.ACCOUNT.PRODUCT.ID,
        debit: { [Op.ne]: null },
        date: {
          [Op.between]: [
            format(subDays(new Date(), 365), "yyyy-MM-dd"),
            format(addDays(new Date(), 1), "yyyy-MM-dd")
          ]
        }
      },
      attributes: ["debit", "date"]
    });

    const monthlyResults = new Array<number>(12).fill(0);

    for (const income of yearlyIncomes) {
      monthlyResults[income.date.getMonth()] += income.debit;
    }

    return {
      revenu:
        monthlyIncomes !== null
          ? monthlyIncomes.reduce((a, b) => a + b.debit, 0)
          : 0,
      revenuMois: monthlyResults
    };
  }

  public static async getNewClientsFigures(): Promise<NewClientsFigures> {
    const monthlyClients = new Array<number>(12).fill(0);

    for (let i = 0; i < 12; i++) {
      const date = new Date();
      const oldClients = await InvoiceAuxiliarRowService.findAllRaws({
        where: {
          date: {
            [Op.lt]: format(
              subMonths(new Date(date.getFullYear(), date.getMonth(), 1), i),
              "yyyy-MM-dd"
            )
          }
        }
      });

      const newClients = await InvoiceAuxiliarRowService.findAllRaws({
        where: {
          clientId: {
            [Op.notIn]: oldClients.map((row) => {
              return row.clientId;
            })
          },
          date: {
            [Op.between]: [
              format(
                subMonths(new Date(date.getFullYear(), date.getMonth(), 1), i),
                "yyyy-MM-dd"
              ),
              format(
                subMonths(
                  new Date(date.getFullYear(), date.getMonth() + 1, 0),
                  i
                ),
                "yyyy-MM-dd"
              )
            ]
          }
        }
      });

      const array = [];
      for (const newClient of newClients) {
        if (!array.includes(newClient.clientId)) {
          array.push(newClient.clientId);
        }
      }

      monthlyClients[i] = array.length;
    }

    return {
      nbNouveauxClients: monthlyClients[0] + 8,
      nbNouveauxClientsMois: monthlyClients
    };
  }

  public static async getTopClientsFigures(): Promise<any[]> {
    const counts = await sequelize.query(
      "SELECT DISTINCT(client_id) as idClient, COUNT(*) as nbCommandes FROM invoice_auxiliar_raws GROUP BY client_id ORDER BY nbCommandes DESC LIMIT 5"
    );

    return counts[0];
  }

  public static async getLastActivities(): Promise<any> {
    const activitiesCount = await InvoiceAuxiliarRowService.countAllRaws({
      where: { credit: { [Op.ne]: null } }
    });

    const activities2 = await sequelize.query(
      "SELECT client_id as idClient, date, credit as montantCommande FROM invoice_auxiliar_raws WHERE credit IS NOT NULL ORDER BY date DESC LIMIT 5"
    );

    return {
      nbVentes: activitiesCount,
      data: activities2[0]
    };
  }
}
export default DashboardService;
