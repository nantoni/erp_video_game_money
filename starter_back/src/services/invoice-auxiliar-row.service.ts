import { Order, WhereOptions, CreateOptions, FindOptions, Op } from "sequelize";

import BaseService from "./base.service";
import { InvoiceAuxiliarRow, InvoiceRaw } from "../models";
import { InvoiceAuxiliarRawCreate } from "../types/models/invoice-auxiliar-row.type";

class InvoiceAuxiliarRowService extends BaseService<InvoiceAuxiliarRow> {
  public static async getBankTransactions(): Promise<any> {
    const transactions = await this.findAll({
      where: {
        debit: {
          [Op.ne]: null
        }
      },
      limit: 100
    });
    const res = [];
    for (const transaction of transactions) {
      res.push({
        date: transaction.date,
        ref: transaction.invoiceId,
        description: `Prélèvement Client n°${transaction.clientId}`,
        montant: transaction.debit
      });
    }

    return res;
  }

  public static async createInvoiceAuxiliarRow(
    rowBody: InvoiceAuxiliarRawCreate
  ): Promise<InvoiceAuxiliarRow> {
    return await this.create(rowBody);
  }

  public static async findOneById(
    id: number
  ): Promise<InvoiceAuxiliarRow | null> {
    return await this.findOne({
      invoiceId: id
    });
  }

  public static async findAllRaws(
    options?: FindOptions
  ): Promise<InvoiceAuxiliarRow[] | null> {
    return await this.findAll(options);
  }

  public static async findAllByInvoiceId(
    id: number
  ): Promise<InvoiceAuxiliarRow[] | null> {
    return await this.findAll({
      where: {
        invoiceId: id
      }
    });
  }

  public static async countAllRaws(options?: FindOptions): Promise<number> {
    return await this.count(options);
  }

  private static async findOne(
    optionsWhere: WhereOptions,
    optionsOrder: Order = [["date", "DESC"]]
  ): Promise<InvoiceAuxiliarRow | null> {
    return InvoiceAuxiliarRow.findOne({
      where: { ...optionsWhere },
      order: optionsOrder
    });
  }

  private static async findAll(
    options: FindOptions,
    optionsOrder: Order = [["date", "DESC"]]
  ): Promise<InvoiceAuxiliarRow[] | null> {
    return InvoiceAuxiliarRow.findAll({
      ...options,
      order: optionsOrder
    });
  }

  private static async create(
    invoiceValues: InvoiceAuxiliarRawCreate,
    options?: CreateOptions
  ): Promise<InvoiceAuxiliarRow> {
    return InvoiceAuxiliarRow.create(invoiceValues, options);
  }

  private static async count(options: FindOptions): Promise<number> {
    return InvoiceAuxiliarRow.count(options);
  }
}
export default InvoiceAuxiliarRowService;
