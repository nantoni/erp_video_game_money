import Joi from "@hapi/joi";

/**
 * @swagger
 * components:
 *  parameters:
 *    StartAt:
 *      in: query
 *      name: startAt
 *      schema:
 *        type: string
 *        format: date
 *      example: "2020-01-01"
 *    EndAt:
 *      in: query
 *      name: endAt
 *      schema:
 *        type: string
 *        format: date
 *      example: "2021-01-01"
 */
const dateRange = (): object => {
  return {
    startAt: Joi.date(),
    endAt: Joi.date().min(Joi.ref("startAt"))
  };
};

const dateRangeRequired = (): object => {
  return {
    startAt: Joi.date().required(),
    endAt: Joi.date().min(Joi.ref("startAt")).required()
  };
};

export { dateRange, dateRangeRequired };
