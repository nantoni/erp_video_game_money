import Joi from "@hapi/joi";

/**
 * @swagger
 * components:
 *  parameters:
 *    InvoiceId:
 *      in: path
 *      name: invoiceId
 *      schema:
 *        type: integer
 *      description: invoice id
 *      example: 1
 *      required: true
 */
const invoiceIdParam = Joi.object({
  params: {
    invoiceId: Joi.number().required()
  }
});

/**
 * @swagger
 * components:
 *  requestBodies:
 *    CreateInvoice:
 *      description: invoice body for invoice create
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              invoiceId:
 *                type: number
 *              clientId:
 *                type: number
 *              date:
 *                type: date
 *              amount:
 *                type: number
 *              VAT:
 *                type: number
 *          example:
 *            invoiceId: 1
 *            clientId: 2
 *            date: 2020-01-01 00:00:00
 *            amount: 120.20
 */
const create = Joi.object({
  body: Joi.array().items({
    invoiceId: Joi.number().required(),
    clientId: Joi.number().required(),
    date: Joi.date().required(),
    amount: Joi.number().required(),
    VAT: Joi.number()
  })
});

export default {
  invoiceIdParam,
  create
};
