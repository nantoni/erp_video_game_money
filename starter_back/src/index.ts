import App from "./app";

import ENV from "./env";

import { initModels, syncDatabase } from "./models";

const init = async (): Promise<void> => {
  initModels();
  await syncDatabase();
};
const port: number = ENV.PORT;
const app = new App(port);

init()
  .then(() => {
    app.listen();
  })
  .catch((err: Error) => {
    console.log(`${err.name} : ${err.message}
      ${err.stack || ""}`);
    throw err;
  });
