import equal from "fast-deep-equal";

const checkArrayDuplicateItems = (items: object[]): boolean => {
  for (let itemIndex = 0; itemIndex < items.length - 1; itemIndex++) {
    for (
      let itemCompareIndex = itemIndex + 1;
      itemCompareIndex < items.length;
      itemCompareIndex++
    ) {
      if (equal(items[itemIndex], items[itemCompareIndex])) {
        return true;
      }
    }
  }
  return false;
};

export default { checkArrayDuplicateItems };
