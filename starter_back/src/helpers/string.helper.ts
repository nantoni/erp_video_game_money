type ForceType = "upper" | "lower" | "capitalize" | "capitalize-all";

const capitalizeOrNull = (s: string | null): string | null => {
  if (!s) {
    return null;
  }

  return `${s.charAt(0).toUpperCase()}${s.slice(1)}`;
};

const capitalizeAllOrNull = (s: string | null): string | null => {
  if (!s) {
    return null;
  }

  return s
    .split(" ")
    .map(
      (word: string): string =>
        `${word.charAt(0).toUpperCase()}${word.substring(1)}`
    )
    .join(" ");
};

const toLowerCaseOrNull = (s: string | null): string | null => {
  if (!s) {
    return null;
  }

  return s.toLowerCase();
};

const toUpperCaseOrNull = (s: string | null): string | null => {
  if (!s) {
    return null;
  }

  return s.toUpperCase();
};

const forceToCase = (s: string | null, force: ForceType): string | null => {
  switch (force) {
    case "upper":
      return toUpperCaseOrNull(s);
    case "lower":
      return toLowerCaseOrNull(s);
    case "capitalize":
      return capitalizeOrNull(s);
    case "capitalize-all":
      return capitalizeAllOrNull(s);
    default:
      return null;
  }
};

export default { forceToCase };
