/*
 * Winston NPM Config levels:
 *  - error: number;
 *  - warn: number;
 *  - info: number;
 *  - http: number;
 *  - verbose: number;
 *  - debug: number;
 *  - silly: number;
 */

import Rollbar from "rollbar";
import { createLogger, format, config, transports } from "winston";
import WinstonTransport from "winston-transport";

import { ClientErrorType } from "../client-error";

import ENV from "../env";
import { GLOBAL } from "../constants";

const rollbar = new Rollbar({
  accessToken: ENV.ROLLBAR_KEY,
  captureUncaught: true,
  captureUnhandledRejections: true,
  enabled:
    ENV.NODE_ENV === GLOBAL.ENV.PRODUCTION ||
    ENV.NODE_ENV === GLOBAL.ENV.DEVELOPMENT
});

const logger = createLogger({
  levels: config.npm.levels,
  format: format.json(),
  transports: [
    new transports.Console({
      level: ENV.LOGGER_LEVEL_CONSOLE,
      format: format.cli(),
      silent: ENV.NODE_ENV === GLOBAL.ENV.TEST && !ENV.LOGGER_FORCE_TEST_CONSOLE
    })
  ]
});

logger.add(
  new WinstonTransport({
    level: ENV.LOGGER_LEVEL_ROLLBAR,
    handleExceptions: true,
    silent:
      ENV.NODE_ENV !== GLOBAL.ENV.DEVELOPMENT &&
      ENV.NODE_ENV !== GLOBAL.ENV.PRODUCTION,
    log: (args: ClientErrorType, next: () => void): void => {
      switch (args.level) {
        case GLOBAL.LOGGER.LOGGER_LEVEL.DEBUG:
          rollbar.debug(args.message);
          break;
        case GLOBAL.LOGGER.LOGGER_LEVEL.INFO:
          rollbar.info(args.message);
          break;
        case GLOBAL.LOGGER.LOGGER_LEVEL.WARN:
          rollbar.warn(args.message);
          break;
        case GLOBAL.LOGGER.LOGGER_LEVEL.ERROR:
          rollbar.error(args.message);
          break;
        default:
          rollbar.log(args.message);
      }

      return next();
    }
  })
);

export default logger;
