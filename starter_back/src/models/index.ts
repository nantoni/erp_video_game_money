import { Sequelize } from "sequelize";

import ENV from "../env";
import { DATABASE_CONFIG } from "../constants";

import Logger from "../helpers/logger.helper";

import BaseModel from "./base.model";
import InvoiceRaw from "./invoice-raw.model";
import InvoiceAuxiliarRow from "./invoice-auxiliar-row.model";

const sequelize: Sequelize = new Sequelize(ENV.DATABASE_URL, {
  ...DATABASE_CONFIG.SEQUELIZE,
  logging:
    ENV.LOGGER_LEVEL_CONSOLE === "debug"
      ? (log: string): void => {
          Logger.debug(log);
        }
      : false
});

interface Models {
  [key: string]: typeof BaseModel;
}

const models: Models = {
  InvoiceRaw,
  InvoiceAuxiliarRow
};

const init = (): void => {
  const values = Object.values(models);
  for (const model of values) {
    model.initModel();
  }

  for (const model of values) {
    model.associate();
  }
};

const syncDatabase = async (): Promise<void> => {
  await sequelize.sync();
};

export {
  sequelize,
  init as initModels,
  syncDatabase,
  BaseModel,
  InvoiceRaw,
  InvoiceAuxiliarRow
};
