import { DataTypes } from "sequelize";

import { sequelize } from "./";

import { ModelInitType } from "../types/models/common.type";
import { AttributesType } from "../types/models/invoice-raw.type";

import { BaseModel } from "./";

class InvoiceRaw extends BaseModel implements AttributesType {
  public invoiceId: number;
  public account: number;
  public description: string;
  public debit: number | null;
  public credit: number | null;
  public date: Date;

  public static associate(): void {}

  public static initModel(): void {
    const invoiceAttributes: ModelInitType<AttributesType> = Object.freeze({
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "id",
        unique: true,
        validate: {
          isInt: true
        }
      },
      invoiceId: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "invoice_id",
        validate: {
          isInt: true
        }
      },
      account: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "account",
        validate: {
          isInt: true
        }
      },
      description: {
        type: new DataTypes.STRING(),
        field: "description"
      },
      debit: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "debit",
        allowNull: true,
        validate: {
          isFloat: true
        }
      },
      credit: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "credit",
        allowNull: true,
        validate: {
          isFloat: true
        }
      },
      date: {
        type: new DataTypes.DATE(),
        allowNull: false,
        validate: {
          isDate: true
        }
      }
    });

    InvoiceRaw.init(invoiceAttributes, {
      tableName: "invoice_raws",
      sequelize
    });
  }
}

export default InvoiceRaw;
