import { DataTypes } from "sequelize";

import { sequelize } from "./";

import { ModelInitType } from "../types/models/common.type";
import { AttributesType } from "../types/models/invoice-auxiliar-row.type";

import { BaseModel } from "./";

class InvoiceAuxiliarRaw extends BaseModel implements AttributesType {
  public invoiceId: number;
  public clientId: number;
  public debit: number | null;
  public credit: number | null;
  public date: Date;

  public static associate(): void {}

  public static initModel(): void {
    const invoiceAttributes: ModelInitType<AttributesType> = Object.freeze({
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "id",
        unique: true,
        validate: {
          isInt: true
        }
      },
      invoiceId: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "invoice_id",
        validate: {
          isInt: true
        }
      },
      clientId: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "client_id",
        validate: {
          isInt: true
        }
      },
      debit: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "debit",
        allowNull: true,
        validate: {
          isFloat: true
        }
      },
      credit: {
        type: new DataTypes.INTEGER().UNSIGNED,
        field: "credit",
        allowNull: true,
        validate: {
          isFloat: true
        }
      },
      date: {
        type: new DataTypes.DATE(),
        allowNull: false,
        validate: {
          isDate: true
        }
      }
    });

    InvoiceAuxiliarRaw.init(invoiceAttributes, {
      tableName: "invoice_auxiliar_raws",
      sequelize
    });
  }
}

export default InvoiceAuxiliarRaw;
