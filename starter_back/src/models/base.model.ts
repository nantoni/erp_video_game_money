import { Model } from "sequelize";

class BaseModel extends Model {
  public static associate(): void {
    return;
  }

  public static initModel(): void {
    return;
  }
}

export default BaseModel;
