import HttpStatus from "http-status-codes";
import { ERROR, GLOBAL } from "./constants";
import { ValidationErrorItem } from "@hapi/joi";

export interface ClientErrorType {
  level?: string;
  message: string;
  name: string | null;
  status?: number | null;
  details?: string | null | any[];
  originalError?: Error | null;
}

export interface ClientReturnedErrorType {
  name: string | null;
  message: string;
  details: string | null | any[];
}

export interface SimpleJoiErrorType {
  field: string;
  message: string;
}

const simplifyJoiError = (
  errors: ValidationErrorItem[]
): SimpleJoiErrorType[] => {
  return errors.map((error: ValidationErrorItem) => {
    return {
      field: error.context.key,
      message: error.message
    };
  });
};

export default class ClientError extends Error implements ClientErrorType {
  level: string;
  message: string;
  name: string | null;
  status: number;
  details: string | null | any[];
  originalError: Error | null;

  constructor(error: ClientErrorType) {
    super();

    this.name = error.name;
    this.message = error.message;
    this.level = error.level || GLOBAL.LOGGER.LOGGER_LEVEL.ERROR;
    this.status = error.status || HttpStatus.INTERNAL_SERVER_ERROR;
    this.details =
      Array.isArray(error.details) && error.name === ERROR.NAME.VALIDATION
        ? simplifyJoiError(error.details)
        : error.details || null;
    this.originalError =
      error.originalError !== undefined ? error.originalError : null;
  }

  public getError(): ClientReturnedErrorType {
    return {
      name: "Error",
      message: this.message,
      details: this.details
    };
  }

  public getLogError(): string {
    let loggerMessageDetail: string;
    if (this.details) {
      loggerMessageDetail = `${JSON.stringify(this.details, null, "\t")}`;
    } else {
      if (!this.originalError) {
        loggerMessageDetail = "";
      } else {
        loggerMessageDetail = this.originalError.stack;
      }
    }

    return `${this.name} - ${this.message} ${loggerMessageDetail}`;
  }
}
