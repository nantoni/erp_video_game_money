// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    moduleFileExtensions: ["js", "jsx", "ts", "node"],
    transform: {
        "^.+\\.(ts|tsx)?$": "ts-jest"
    },
    rootDir: "./",
    moduleNameMapper: {
        "^@/(.*)$": "<rootDir>/$1"
    },
    testMatch: ["<rootDir>/src/__tests__/**/*.test.ts?(x)"],
    globals: {
        "ts-jest": {
            babel: true,
            tsConfig: "tsconfig.json"
        }
    },
    globalSetup: "<rootDir>/src/__tests__/tests-config/test-setup.ts",
    setupFilesAfterEnv: [
        "<rootDir>/src/__tests__/tests-config/test-setup-after-env.ts"
    ]
};
