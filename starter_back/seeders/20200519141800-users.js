"use strict";

// password is "password123"

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "users",
      [
        {
          uid: "1478308a-9aa5-11ea-bb37-0242ac130002",
          email: "admin@example.fr",
          password:
            "$2a$11$r5nxQK53TTeDKVFFfKkRIOMLMdPeGnu3ex8jXAeoUNJ4Zg33eWsr2",
          phone: "+33608410102",
          first_name: "François",
          last_name: "Admin",
          role: "admin",
          ip_address: "127.0.0.1",
          enabled: true,
          created_at: "2020-01-01 12:00:00"
        },
        {
          uid: "ae0617c6-9aa5-11ea-bb37-0242ac130002",
          email: "user@example.fr",
          password:
            "$2a$11$r5nxQK53TTeDKVFFfKkRIOMLMdPeGnu3ex8jXAeoUNJ4Zg33eWsr2",
          phone: "+33608410102",
          first_name: "Francois",
          last_name: "User",
          role: "user",
          ip_address: "127.0.0.1",
          enabled: true,
          created_at: "2020-01-01 12:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  }
};
