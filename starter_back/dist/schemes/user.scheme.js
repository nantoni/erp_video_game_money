"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userDefault = exports.USER_SCHEME = void 0;
/**
 * @category Models
 *
 * @swagger
 * components:
 *    schemas:
 *      UserDefault:
 *        type: object
 *        properties:
 *          uid:
 *            type: string
 *          email:
 *            type: string
 *          firstName:
 *            type: string
 *          lastName:
 *            type: string
 *          phone:
 *            type: string
 *          role:
 *            type: string
 *      UserAdmin:
 *        allOf:
 *          - $ref: '#/components/schemas/UserDefault'
 *          - type: object
 *            properties:
 *              userId:
 *                type: number
 *              ipAddress:
 *                type: string
 *              enabled:
 *                type: boolean
 *              createdAt:
 *                type: string
 *                format: date-time
 *              updatedAt:
 *                type: string
 *                format: date-time
 *              deletedAt:
 *                type: string
 *                format: date-time
 */
/**
 * @swagger
 * components:
 *  examples:
 *    UserAdmin:
 *      value:
 *        userId: 1
 *        uid: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"
 *        email: "adrien@vazee.fr"
 *        firstName: "Adrien"
 *        lastName: "Famille"
 *        phone: "+33601010101"
 *        role: "admin"
 *        ipAddress: "127.0.0.1"
 *        enabled: true
 *        createdAt: "2020-01-01T12:00:00.000Z"
 *        updatedAt: null
 *        deletedAt: null
 */
const userDefault = [
    "uid",
    "email",
    "phone",
    "firstName",
    "lastName",
    "role"
];
exports.userDefault = userDefault;
const userSchemeType = Object.freeze({
    DEFAULT: {
        LOGIN: {
            include: userDefault
        }
    },
    ADMIN: {
        FIND: {
            include: [
                ...userDefault,
                "userId",
                "ipAddress",
                "enabled",
                "createdAt",
                "updatedAt",
                "deletedAt"
            ]
        }
    }
});
exports.USER_SCHEME = userSchemeType;
//# sourceMappingURL=user.scheme.js.map