"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const client_error_1 = __importDefault(require("../client-error"));
const constants_1 = require("../constants");
const JoiValidationOptions = {
    abortEarly: false,
    allowUnknown: false,
    stripUnknown: false
};
exports.default = (schema) => {
    return (req, res, next) => {
        const toValidate = Object.assign(Object.assign(Object.assign({}, (Object.entries(req.params).length > 0 && { params: req.params })), (req.body &&
            Object.entries(req.body).length > 0 && { body: req.body })), (Object.entries(req.query).length > 0 && { query: req.query }));
        const { error: validationError } = schema.validate(toValidate, JoiValidationOptions);
        const valid = validationError == null;
        if (valid) {
            next();
        }
        else {
            const { details } = validationError;
            next(new client_error_1.default({
                name: constants_1.ERROR.NAME.VALIDATION,
                level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.WARN,
                message: constants_1.ERROR.JOI_VALIDATION,
                status: http_status_codes_1.default.BAD_REQUEST,
                details,
                originalError: validationError
            }));
        }
    };
};
//# sourceMappingURL=validation.middleware.js.map