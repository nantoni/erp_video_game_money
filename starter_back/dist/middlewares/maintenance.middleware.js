"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const client_error_1 = __importDefault(require("../client-error"));
const constants_1 = require("../constants");
const env_1 = __importDefault(require("../env"));
exports.default = () => {
    return (req, res, next) => {
        if (env_1.default.IS_UNDER_MAINTENANCE) {
            next(new client_error_1.default({
                name: constants_1.ERROR.NAME.MAINTENANCE,
                level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.SERVICE_UNAVAILABLE,
                message: constants_1.ERROR.MAINTENANCE.IS_ON
            }));
        }
        else {
            next();
        }
    };
};
//# sourceMappingURL=maintenance.middleware.js.map