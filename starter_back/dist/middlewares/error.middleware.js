"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const sequelize_1 = require("sequelize");
const client_error_1 = __importDefault(require("../client-error"));
const logger_helper_1 = __importDefault(require("../helpers/logger.helper"));
const constants_1 = require("../constants");
const sequelizeErrorHandler = (err, req, res, next) => {
    let clientError;
    if (err instanceof sequelize_1.ValidationError) {
        clientError = new client_error_1.default({
            name: err.name,
            message: err.message,
            level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
            status: http_status_codes_1.default.INTERNAL_SERVER_ERROR,
            details: err.errors,
            originalError: err
        });
    }
    if (err.constructor.name === "EagerLoadingError") {
        clientError = new client_error_1.default({
            name: err.name,
            message: err.message,
            level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
            status: http_status_codes_1.default.INTERNAL_SERVER_ERROR,
            originalError: err
        });
        next(clientError);
    }
    next(err);
};
const otherErrorHandler = (err, req, res, next) => {
    if (!(err instanceof client_error_1.default) && err instanceof Error) {
        next(new client_error_1.default({
            name: err.name,
            message: err.message,
            level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
            status: http_status_codes_1.default.INTERNAL_SERVER_ERROR,
            originalError: err
        }));
    }
    else if (!(err instanceof client_error_1.default)) {
        console.log("____add error handler____", err instanceof Error, err.constructor.name, err);
    }
    next(err);
};
const logErrors = (err, req, res, next) => {
    logger_helper_1.default.log(err.level, err.getLogError());
    next(err);
};
const errorHandler = (err, req, res, _next) => {
    res.status(err.status).send({ error: err.getError() });
};
exports.default = {
    sequelizeErrorHandler,
    otherErrorHandler,
    logErrors,
    errorHandler
};
//# sourceMappingURL=error.middleware.js.map