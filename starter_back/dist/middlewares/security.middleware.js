"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const client_error_1 = __importDefault(require("../client-error"));
const constants_1 = require("../constants");
exports.default = (...roles) => {
    return (req, res, next) => {
        if (!res.locals || !res.locals.user || !res.locals.user.userId) {
            return next(new client_error_1.default({
                name: constants_1.ERROR.NAME.AUTHENTICATION,
                level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.UNAUTHORIZED,
                message: constants_1.ERROR.AUTHENTICATION.UNAUTHORIZED
            }));
        }
        const authorized = roles.includes(res.locals.user.role);
        if (res.locals.user.role !== constants_1.USER.ROLE.ENUM.ADMIN && authorized !== true) {
            next(new client_error_1.default({
                name: constants_1.ERROR.NAME.HAS_ANY_ROLE,
                level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.FORBIDDEN,
                message: constants_1.ERROR.HAS_ANY_ROLE.FORBIDDEN
            }));
        }
        return next();
    };
};
//# sourceMappingURL=security.middleware.js.map