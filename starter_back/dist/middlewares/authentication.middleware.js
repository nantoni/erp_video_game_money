"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const security_helper_1 = __importDefault(require("../helpers/security.helper"));
const client_error_1 = __importDefault(require("../client-error"));
const constants_1 = require("../constants");
security_helper_1.default.initPassportUse();
exports.default = (required = true) => {
    return (req, res, next) => {
        passport_1.default.authenticate("jwt", { session: false }, (err, user) => {
            if (err) {
                return next(err);
            }
            if (required && !user) {
                return next(new client_error_1.default({
                    name: constants_1.ERROR.NAME.AUTHENTICATION,
                    level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                    status: http_status_codes_1.default.UNAUTHORIZED,
                    message: constants_1.ERROR.AUTHENTICATION.UNAUTHORIZED
                }));
            }
            res.locals.user = user;
            return next();
        })(req, res, next);
    };
};
//# sourceMappingURL=authentication.middleware.js.map