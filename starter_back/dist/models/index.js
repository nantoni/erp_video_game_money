"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceAuxiliarRow = exports.InvoiceRaw = exports.BaseModel = exports.syncDatabase = exports.initModels = exports.sequelize = void 0;
const sequelize_1 = require("sequelize");
const env_1 = __importDefault(require("../env"));
const constants_1 = require("../constants");
const logger_helper_1 = __importDefault(require("../helpers/logger.helper"));
const base_model_1 = __importDefault(require("./base.model"));
exports.BaseModel = base_model_1.default;
const invoice_raw_model_1 = __importDefault(require("./invoice-raw.model"));
exports.InvoiceRaw = invoice_raw_model_1.default;
const invoice_auxiliar_row_model_1 = __importDefault(require("./invoice-auxiliar-row.model"));
exports.InvoiceAuxiliarRow = invoice_auxiliar_row_model_1.default;
const sequelize = new sequelize_1.Sequelize(env_1.default.DATABASE_URL, Object.assign(Object.assign({}, constants_1.DATABASE_CONFIG.SEQUELIZE), { logging: env_1.default.LOGGER_LEVEL_CONSOLE === "debug"
        ? (log) => {
            logger_helper_1.default.debug(log);
        }
        : false }));
exports.sequelize = sequelize;
const models = {
    InvoiceRaw: invoice_raw_model_1.default,
    InvoiceAuxiliarRow: invoice_auxiliar_row_model_1.default
};
const init = () => {
    const values = Object.values(models);
    for (const model of values) {
        model.initModel();
    }
    for (const model of values) {
        model.associate();
    }
};
exports.initModels = init;
const syncDatabase = async () => {
    await sequelize.sync();
};
exports.syncDatabase = syncDatabase;
//# sourceMappingURL=index.js.map