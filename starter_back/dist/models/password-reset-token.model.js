"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const moment_1 = __importDefault(require("moment"));
const _1 = require("./");
const constants_1 = require("../constants");
const _2 = require("./");
const millisecondsInOneHour = 3600000;
class PasswordResetToken extends _2.BaseModel {
    static associate() {
        PasswordResetToken.belongsTo(_2.User, {
            foreignKey: "userId",
            constraints: false,
            as: "user",
            onDelete: "CASCADE"
        });
    }
    static initModel() {
        const passwordResetAttributes = Object.freeze({
            passwordResetTokenId: {
                autoIncrement: true,
                primaryKey: true,
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "id",
                unique: true,
                validate: {
                    isInt: true
                }
            },
            userId: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                allowNull: false,
                validate: {
                    isInt: true
                }
            },
            token: {
                type: sequelize_1.DataTypes.UUID,
                unique: true,
                allowNull: false,
                defaultValue: sequelize_1.DataTypes.UUIDV4,
                validate: {
                    isUUID: 4
                }
            },
            expiresAt: {
                type: new sequelize_1.DataTypes.DATE(),
                allowNull: false,
                defaultValue: () => new Date(Date.now() +
                    constants_1.PASSWORD_RESET_TOKEN.EXPIRES_AT.HOURS * millisecondsInOneHour),
                validate: {
                    isDate: true
                }
            },
            used: {
                type: sequelize_1.DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
                validate: {
                    isIn: [["true", "false"]]
                }
            },
            createdAt: {
                type: new sequelize_1.DataTypes.DATE(),
                allowNull: false,
                defaultValue: sequelize_1.DataTypes.NOW,
                validate: {
                    isDate: true
                }
            },
            updatedAt: {
                type: new sequelize_1.DataTypes.DATE(),
                allowNull: true,
                defaultValue: null,
                validate: {
                    isDate: true
                }
            },
            hasExpired: {
                type: new sequelize_1.DataTypes.VIRTUAL(sequelize_1.DataTypes.BOOLEAN, ["expiresAt"]),
                get() {
                    return (moment_1.default().isAfter(moment_1.default(this.get("expiresAt"), "YYYY-MM-DD HH:mm:ss")) || this.get("used"));
                }
            }
        });
        PasswordResetToken.init(passwordResetAttributes, {
            tableName: "password_reset_token",
            paranoid: false,
            timestamps: true,
            sequelize: _1.sequelize
        });
    }
}
exports.default = PasswordResetToken;
//# sourceMappingURL=password-reset-token.model.js.map