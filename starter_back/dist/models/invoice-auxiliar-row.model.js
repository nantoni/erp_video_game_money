"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const _1 = require("./");
const _2 = require("./");
class InvoiceAuxiliarRaw extends _2.BaseModel {
    static associate() { }
    static initModel() {
        const invoiceAttributes = Object.freeze({
            id: {
                autoIncrement: true,
                primaryKey: true,
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "id",
                unique: true,
                validate: {
                    isInt: true
                }
            },
            invoiceId: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "invoice_id",
                validate: {
                    isInt: true
                }
            },
            clientId: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "client_id",
                validate: {
                    isInt: true
                }
            },
            debit: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "debit",
                allowNull: true,
                validate: {
                    isFloat: true
                }
            },
            credit: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "credit",
                allowNull: true,
                validate: {
                    isFloat: true
                }
            },
            date: {
                type: new sequelize_1.DataTypes.DATE(),
                allowNull: false,
                validate: {
                    isDate: true
                }
            }
        });
        InvoiceAuxiliarRaw.init(invoiceAttributes, {
            tableName: "invoice_auxiliar_raws",
            sequelize: _1.sequelize
        });
    }
}
exports.default = InvoiceAuxiliarRaw;
//# sourceMappingURL=invoice-auxiliar-row.model.js.map