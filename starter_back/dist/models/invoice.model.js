"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const _1 = require("./");
const _2 = require("./");
class InvoiceRaw extends _2.BaseModel {
    static associate() { }
    static initModel() {
        const invoiceAttributes = Object.freeze({
            id: {
                autoIncrement: true,
                primaryKey: true,
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "id",
                unique: true,
                validate: {
                    isInt: true
                }
            },
            invoiceId: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "invoice_id",
                validate: {
                    isInt: true
                }
            },
            clientId: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "client_id",
                validate: {
                    isInt: true
                }
            },
            account: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "account",
                validate: {
                    isInt: true
                }
            },
            description: {
                type: new sequelize_1.DataTypes.STRING(),
                field: "description"
            },
            debit: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "debit",
                allowNull: true,
                validate: {
                    isInt: true
                }
            },
            credit: {
                type: new sequelize_1.DataTypes.INTEGER().UNSIGNED,
                field: "credit",
                allowNull: true,
                validate: {
                    isInt: true
                }
            },
            date: {
                type: new sequelize_1.DataTypes.DATE(),
                allowNull: false,
                validate: {
                    isDate: true
                }
            }
        });
        InvoiceRaw.init(invoiceAttributes, {
            tableName: "invoice_raws",
            paranoid: true,
            timestamps: true,
            sequelize: _1.sequelize
        });
    }
}
exports.default = InvoiceRaw;
//# sourceMappingURL=invoice.model.js.map