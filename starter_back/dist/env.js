"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const envPath = process.env.NODE_ENV === "test" ? "./.env.test" : "./.env";
dotenv_1.default.config({
    path: envPath
});
const ENV = Object.freeze({
    NODE_ENV: process.env.NODE_ENV,
    API_VERSION: process.env.API_VERSION,
    PORT: Number(process.env.PORT),
    IS_UNDER_MAINTENANCE: process.env.MAINTENANCE_ENABLED === "true",
    DATABASE_URL: process.env.DATABASE_URL,
    DATABASE_MAX_CONNECTION: Number(process.env.DATABASE_MAX_CONNECTION),
    LOGGER_LEVEL_CONSOLE: process.env.LOGGER_LEVEL_CONSOLE,
    LOGGER_LEVEL_ROLLBAR: process.env.LOGGER_LEVEL_ROLLBAR,
    LOGGER_FORCE_TEST_CONSOLE: process.env.LOGGER_FORCE_TEST_CONSOLE === "true",
    RATE_LIMITER_WINDOW_MS: process.env.RATE_LIMITER_WINDOW_MS,
    RATE_LIMITER_MAX_COUNT: process.env.RATE_LIMITER_MAX_COUNT,
    ROLLBAR_KEY: process.env.ROLLBAR_KEY
});
for (const [key, value] of Object.entries(ENV)) {
    if (value === undefined || value === "") {
        throw new Error(`Config error, undefined ENV: ${key}`);
    }
}
exports.default = ENV;
//# sourceMappingURL=env.js.map