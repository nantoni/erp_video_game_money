"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
/**
 * @swagger
 * components:
 *  parameters:
 *    InvoiceId:
 *      in: path
 *      name: invoiceId
 *      schema:
 *        type: integer
 *      description: invoice id
 *      example: 1
 *      required: true
 */
const invoiceIdParam = joi_1.default.object({
    params: {
        invoiceId: joi_1.default.number().required()
    }
});
/**
 * @swagger
 * components:
 *  requestBodies:
 *    CreateInvoice:
 *      description: invoice body for invoice create
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              invoiceId:
 *                type: number
 *              clientId:
 *                type: number
 *              date:
 *                type: date
 *              amount:
 *                type: number
 *              VAT:
 *                type: number
 *          example:
 *            invoiceId: 1
 *            clientId: 2
 *            date: 2020-01-01 00:00:00
 *            amount: 120.20
 */
const create = joi_1.default.object({
    body: joi_1.default.array().items({
        invoiceId: joi_1.default.number().required(),
        clientId: joi_1.default.number().required(),
        date: joi_1.default.date().required(),
        amount: joi_1.default.number().required(),
        VAT: joi_1.default.number()
    })
});
exports.default = {
    invoiceIdParam,
    create
};
//# sourceMappingURL=invoice.validation.js.map