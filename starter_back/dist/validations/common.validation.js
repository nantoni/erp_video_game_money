"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dateRangeRequired = exports.dateRange = void 0;
const joi_1 = __importDefault(require("@hapi/joi"));
/**
 * @swagger
 * components:
 *  parameters:
 *    StartAt:
 *      in: query
 *      name: startAt
 *      schema:
 *        type: string
 *        format: date
 *      example: "2020-01-01"
 *    EndAt:
 *      in: query
 *      name: endAt
 *      schema:
 *        type: string
 *        format: date
 *      example: "2021-01-01"
 */
const dateRange = () => {
    return {
        startAt: joi_1.default.date(),
        endAt: joi_1.default.date().min(joi_1.default.ref("startAt"))
    };
};
exports.dateRange = dateRange;
const dateRangeRequired = () => {
    return {
        startAt: joi_1.default.date().required(),
        endAt: joi_1.default.date().min(joi_1.default.ref("startAt")).required()
    };
};
exports.dateRangeRequired = dateRangeRequired;
//# sourceMappingURL=common.validation.js.map