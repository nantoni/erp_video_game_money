"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("@hapi/joi"));
const constants_1 = require("../constants");
/**
 * @swagger
 * components:
 *  requestBodies:
 *    LoginUser:
 *      description: user credentials
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                format: email
 *              password:
 *                type: string
 *                format: password
 *            required:
 *              - email
 *              - password
 *          examples:
 *            LoginUser:
 *              value:
 *                email: user@example.fr
 *                password: password123
 *            LoginUserAdmin:
 *              value:
 *                email: admin@example.fr
 *                password: password123
 */
const login = joi_1.default.object({
    body: {
        email: joi_1.default.string()
            .trim()
            .lowercase()
            .email()
            .max(constants_1.USER.EMAIL.MAX_LENGTH)
            .required(),
        password: joi_1.default.string()
            .min(constants_1.USER.PASSWORD.MIN_LENGTH)
            .max(constants_1.USER.PASSWORD.MAX_LENGTH)
            .required()
    }
});
exports.default = {
    login
};
//# sourceMappingURL=auth.validation.js.map