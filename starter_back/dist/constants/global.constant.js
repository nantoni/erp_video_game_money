"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GLOBAL = void 0;
exports.GLOBAL = Object.freeze({
    LOCALE: "fr",
    ENV: {
        LOCAL: "local",
        TEST: "test",
        DEVELOPMENT: "develop",
        PRODUCTION: "production"
    },
    LOGGER: {
        LOGGER_LEVEL: {
            WARN: "warn",
            ERROR: "error",
            INFO: "info",
            DEBUG: "debug"
        }
    },
    SECURITY: {
        JWT_ALGORITHMS: [
            "HS256",
            "HS384",
            "HS512",
            "RS256",
            "RS384",
            "RS512",
            "ES256",
            "ES384",
            "ES512",
            "PS256",
            "PS384",
            "PS512",
            "none"
        ],
        DEFAULT_JWT_ALGORITHM: "none",
        SALT_ROUNDS: 11,
        ENCRYPT_ALGORITHM: "aes-192-cbc",
        ENCRYPT_IV_LENGTH: 16 // MUST BE 16 FOR AES ALGORITHM
    },
    LIMIT: {
        DEFAULT: 10,
        MAX: 100
    }
});
//# sourceMappingURL=global.constant.js.map