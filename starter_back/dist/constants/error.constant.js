"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERROR = void 0;
exports.ERROR = Object.freeze({
    NAME: {
        MAINTENANCE: "MAINTENANCE",
        AUTHENTICATION: "AUTHENTICATION",
        VALIDATION: "VALIDATION",
        ROUTER: "ROUTER",
        HAS_ANY_ROLE: "HAS_ANY_ROLE",
        INVOICE: "INVOICE"
    },
    JOI_VALIDATION: "VALIDATION",
    MAINTENANCE: {
        IS_ON: "MAINTENANCE_IS_ON"
    },
    HAS_ANY_ROLE: {
        FORBIDDEN: "HAS_ANY_ROLE_FORBIDDEN"
    },
    AUTHENTICATION: {
        UNAUTHORIZED: "AUTHENTICATION_UNAUTHORIZED",
        FORBIDDEN: "AUTHENTICATION_FORBIDDEN"
    },
    INVOICE: {
        NOT_FOUND: "INVOICE_NOT_FOUND",
        ALREADY_EXISTS: "INVOICE_ALREADY_EXISTS"
    }
});
//# sourceMappingURL=error.constant.js.map