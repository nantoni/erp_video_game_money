"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SCHEME = Object.freeze({
    USER: {
        LOGIN: "LOGIN",
        FIND: "FIND"
    }
});
exports.default = SCHEME;
//# sourceMappingURL=scheme.constant.js.map