"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const env_1 = __importDefault(require("../env"));
const SEQUELIZE = {
    dialect: "mysql",
    timezone: "+00:00",
    pool: {
        min: 0,
        max: env_1.default.DATABASE_MAX_CONNECTION,
        acquire: 30000,
        idle: 10000
    },
    query: {
        raw: false
    },
    retry: {
        match: [
            sequelize_1.TimeoutError,
            sequelize_1.ConnectionError,
            sequelize_1.ConnectionRefusedError,
            sequelize_1.HostNotReachableError,
            sequelize_1.ConnectionTimedOutError
        ],
        max: 1
    },
    define: {
        underscored: true,
        freezeTableName: true,
        charset: "utf8",
        collate: "utf8_general_ci",
        timestamps: false
    }
};
const DATABASE_CONFIG = Object.freeze({
    SEQUELIZE
});
exports.default = DATABASE_CONFIG;
//# sourceMappingURL=sequelize.constant.js.map