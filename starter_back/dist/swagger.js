"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_jsdoc_1 = __importDefault(require("swagger-jsdoc"));
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const logger_helper_1 = __importDefault(require("./helpers/logger.helper"));
const swagger_base_config_json_1 = __importDefault(require("./swagger-base.config.json"));
const INDENT = 2;
const build = () => {
    const swagger = swagger_jsdoc_1.default(Object.assign(Object.assign({}, swagger_base_config_json_1.default), { apis: [
            `${__dirname}/constants/*.constant.*`,
            `${__dirname}/models/*.model.*`,
            `${__dirname}/routes/*.route.*`,
            `${__dirname}/validations/*.validation.*`,
            `${__dirname}/schemes/*.scheme.*`
        ] }));
    try {
        const file = JSON.stringify(swagger, null, INDENT);
        fs.writeFileSync(path.resolve(__dirname, "openapi.json"), file, "utf8");
    }
    catch (err) {
        logger_helper_1.default.log(err);
    }
    return swagger;
};
exports.default = build;
//# sourceMappingURL=swagger.js.map