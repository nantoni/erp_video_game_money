"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const express_async_handler_1 = __importDefault(require("express-async-handler"));
const middlewares_1 = require("../middlewares");
const constants_1 = require("../constants");
const client_error_1 = __importDefault(require("../client-error"));
const services_1 = require("../services");
const validations_1 = require("../validations");
const router = express_1.Router();
/**
 * @swagger
 * /invoices:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get all invoice rows"
 *    description: "Get invoices"
 *    operationId: "GetInvoices"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Invoice'
 */
router.get("/", [], express_async_handler_1.default(async (req, res) => {
    const invoice = await services_1.InvoiceService.findAllRaws({
        limit: 100
    });
    res.status(http_status_codes_1.default.OK).json(invoice);
}));
/**
 * @swagger
 * /invoices/auxiliar:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get invoice auxiliar rows"
 *    description: "Get invoice auxiliar rows"
 *    operationId: "GetInvoiceAuxiliar"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/InvoiceRow'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/InvoiceRow'
 */
router.get("/auxiliar", [], express_async_handler_1.default(async (req, res) => {
    const invoiceRows = await services_1.InvoiceAuxiliarRowService.findAllRaws({ limit: 100 });
    res.status(http_status_codes_1.default.OK).json(invoiceRows);
}));
/**
 * @swagger
 * /invoices:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get all bank rows"
 *    description: "Get bank rows"
 *    operationId: "GetBankRows"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankRows'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/BankRows'
 */
router.get("/bank", [], express_async_handler_1.default(async (req, res) => {
    const bankTransactions = await services_1.InvoiceAuxiliarRowService.getBankTransactions();
    res.status(http_status_codes_1.default.OK).json(bankTransactions);
}));
/**
 * @swagger
 * /invoices/{invoiceId}:
 *  get:
 *    tags:
 *      - "Invoices"
 *    summary: "get an invoice from invoiceId"
 *    description: "Get invoice"
 *    operationId: "GetInvoice"
 *    parameters:
 *      - $ref: '#/components/parameters/InvoiceId'
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Invoice'
 *      404:
 *        description: "Invoice not found"
 */
router.get("/:invoiceId", [middlewares_1.validationMiddleware(validations_1.InvoiceValidation.invoiceIdParam)], express_async_handler_1.default(async (req, res) => {
    const invoiceId = parseInt(req.params.invoiceId, 10);
    const invoice = await services_1.InvoiceService.findAllByInvoiceId(invoiceId);
    if (invoice === null) {
        throw new client_error_1.default({
            status: http_status_codes_1.default.NOT_FOUND,
            name: constants_1.ERROR.NAME.INVOICE,
            message: constants_1.ERROR.INVOICE.NOT_FOUND,
            originalError: new Error("invoice.route.ts > GET invoices{invoiceId}")
        });
    }
    res.status(http_status_codes_1.default.OK).json(invoice);
}));
/**
 * @swagger
 * /invoices:
 *  post:
 *    tags:
 *      - "Invoices"
 *    summary: "add invoices"
 *    description: "Add new invoices from body"
 *    operationId: "PostInvoices"
 *    requestBody:
 *      $ref: '#/components/requestBodies/CreateInvoice'
 *    responses:
 *      200:
 *        description: "created"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/InvoiceDefault'
 *            examples:
 *              User:
 *                $ref: '#/components/examples/Invoice'
 *      400:
 *        description: "JOI Validation Error, wrong param and/or body passed"
 */
router.post("/", [middlewares_1.validationMiddleware(validations_1.InvoiceValidation.create)], express_async_handler_1.default(async (req, res) => {
    for (const invoiceBody of req.body) {
        await services_1.InvoiceService.createInvoice(invoiceBody);
    }
    res.status(http_status_codes_1.default.OK).json();
}));
exports.default = router;
//# sourceMappingURL=invoice.route.js.map