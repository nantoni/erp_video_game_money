"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const express_async_handler_1 = __importDefault(require("express-async-handler"));
const services_1 = require("../services");
const router = express_1.Router();
/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get weekly incomes figures"
 *    description: "Get weekly incomes"
 *    operationId: "GetWeeklyIncomes"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/WeeklyIncomes'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/WeeklyIncomes'
 */
router.get("/weekly", [], express_async_handler_1.default(async (req, res) => {
    res.status(http_status_codes_1.default.OK).json({
        statistiquesSemaine: await services_1.DashboardService.getWeeklyIncomes()
    });
}));
/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get monthly incomes figures"
 *    description: "Get weekly incomes"
 *    operationId: "GetWeeklyIncomes"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/MonthlyIncomes'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/MonthlyIncomes'
 */
router.get("/monthly", [], express_async_handler_1.default(async (req, res) => {
    res.status(http_status_codes_1.default.OK).json({
        revenuMensuel: await services_1.DashboardService.getMonthlyIncomes()
    });
}));
/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get list of new client for last month"
 *    description: "Get new clients"
 *    operationId: "GetNewClients"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Clients'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Clients'
 */
router.get("/new-clients", [], express_async_handler_1.default(async (req, res) => {
    res.status(http_status_codes_1.default.OK).json({
        revenuMensuel: await services_1.DashboardService.getNewClientsFigures()
    });
}));
/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get top clients for last month"
 *    description: "Get top clients"
 *    operationId: "GetTopClients"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Client'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Client'
 */
router.get("/top-clients", [], express_async_handler_1.default(async (req, res) => {
    res.status(http_status_codes_1.default.OK).json({
        topClients: await services_1.DashboardService.getTopClientsFigures()
    });
}));
/**
 * @swagger
 * /dashboard:
 *  get:
 *    tags:
 *      - "Dashboard"
 *    summary: "get last activities registered in database"
 *    description: "Get activities"
 *    operationId: "GetActivities"
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Activities'
 *            examples:
 *              Invoice:
 *                $ref: '#/components/examples/Activities'
 */
router.get("/activities", [], express_async_handler_1.default(async (req, res) => {
    res.status(http_status_codes_1.default.OK).json({
        dernieresActivites: await services_1.DashboardService.getLastActivities()
    });
}));
exports.default = router;
//# sourceMappingURL=dashboard.route.js.map