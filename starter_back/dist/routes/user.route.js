"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const express_async_handler_1 = __importDefault(require("express-async-handler"));
const middlewares_1 = require("../middlewares");
const constants_1 = require("../constants");
const client_error_1 = __importDefault(require("../client-error"));
const services_1 = require("../services");
const validations_1 = require("../validations");
const router = express_1.Router();
/**
 * @swagger
 * /vz-admin/users/{userId}:
 *  get:
 *    tags:
 *      - "Admin - Users"
 *    summary: "get user from userId"
 *    description: "Get user"
 *    operationId: "GetUser"
 *    parameters:
 *      - $ref: '#/components/parameters/UserId'
 *    security:
 *      - bearerAuth: []
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserAdmin'
 *            examples:
 *              UserAdmin:
 *                $ref: '#/components/examples/UserAdmin'
 *      400:
 *        description: "Validation Error, wrong body passed"
 *      401:
 *        description: "Unauthorized, authentication failed"
 *      403:
 *        description: "Forbidden, user not granted"
 *      404:
 *        description: "User not found"
 */
router.get("/:invoiceId", [middlewares_1.validationMiddleware(validations_1.UserValidation.userIdParam)], express_async_handler_1.default(async (req, res) => {
    const userId = parseInt(req.params.userId, 10);
    const invoice = await services_1.UserService.findOneById(userId);
    if (invoice === null) {
        throw new client_error_1.default({
            status: http_status_codes_1.default.NOT_FOUND,
            name: constants_1.ERROR.NAME.INVOICE,
            message: constants_1.ERROR.INVOICE.NOT_FOUND,
            originalError: new Error("invoice.route.ts > GET /vz-admin/users/{userId}")
        });
    }
    res.status(http_status_codes_1.default.OK).json(invoice);
}));
/**
 * @swagger
 * /vz-admin/users:
 *  post:
 *    tags:
 *      - "Admin - Users"
 *    summary: "add a user"
 *    description: "Add a new user from body"
 *    operationId: "adminPostUser"
 *    requestBody:
 *      $ref: '#/components/requestBodies/CreateUser'
 *    security:
 *      - bearerAuth: []
 *    responses:
 *      200:
 *        description: "created"
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserDefault'
 *            examples:
 *              User:
 *                $ref: '#/components/examples/User'
 *      400:
 *        description: "JOI Validation Error, wrong param and/or body passed"
 *      401:
 *        description: "Unauthorized, authentication failed"
 *      403:
 *        description: "Forbidden, user not granted"
 *      404:
 *        description: "Menu not found"
 */
router.post("/", [middlewares_1.validationMiddleware(validations_1.UserValidation.create)], express_async_handler_1.default(async (req, res) => {
    const invoice = await services_1.UserService.createUser(req.body);
    res.status(http_status_codes_1.default.OK).json(invoice);
}));
exports.default = router;
//# sourceMappingURL=user.route.js.map
