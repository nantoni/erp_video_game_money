"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const invoice_route_1 = __importDefault(require("./invoice.route"));
const dashboard_route_1 = __importDefault(require("./dashboard.route"));
const routes = [
    {
        path: "invoices",
        router: invoice_route_1.default
    },
    {
        path: "dashboard",
        router: dashboard_route_1.default
    }
];
exports.default = routes;
//# sourceMappingURL=index.js.map