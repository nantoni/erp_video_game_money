"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const express_async_handler_1 = __importDefault(require("express-async-handler"));
const express_1 = require("express");
const constants_1 = require("../constants");
const middlewares_1 = require("../middlewares");
const security_helper_1 = __importDefault(require("../helpers/security.helper"));
const validations_1 = require("../validations");
const services_1 = require("../services");
const scheme_helper_1 = require("../helpers/scheme.helper");
const user_scheme_1 = require("../schemes/user.scheme");
const router = express_1.Router();
/**
 * @swagger
 * /login:
 *  post:
 *    tags:
 *      - "Auth"
 *    summary: "login a user"
 *    description: "Login a user"
 *    operationId: "loginUser"
 *    requestBody:
 *      $ref: '#/components/requestBodies/LoginUser'
 *    responses:
 *      200:
 *        description: "OK"
 *        content:
 *          application/json:
 *            schema:
 *              properties:
 *                user:
 *                  $ref: '#/components/schemas/UserDefault'
 *                token:
 *                  type: string
 *                  example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c
 *      401:
 *        description: "Unauthorized, authentication failed"
 *      403:
 *        description: "Forbidden, user has been disabled"
 */
router.post("/login", [middlewares_1.validationMiddleware(validations_1.AuthValidation.login)], express_async_handler_1.default(async (req, res) => {
    const user = await services_1.UserAuthService.login(req.body);
    const token = security_helper_1.default.generateToken(user.userId);
    res.status(http_status_codes_1.default.OK).json({
        user: scheme_helper_1.serialize(user, scheme_helper_1.getSchemeByRole(user_scheme_1.USER_SCHEME, constants_1.SCHEME.USER.LOGIN, user.role)),
        token
    });
}));
exports.default = router;
//# sourceMappingURL=auth.route.js.map