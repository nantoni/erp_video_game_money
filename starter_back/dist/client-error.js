"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const constants_1 = require("./constants");
const simplifyJoiError = (errors) => {
    return errors.map((error) => {
        return {
            field: error.context.key,
            message: error.message
        };
    });
};
class ClientError extends Error {
    constructor(error) {
        super();
        this.name = error.name;
        this.message = error.message;
        this.level = error.level || constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR;
        this.status = error.status || http_status_codes_1.default.INTERNAL_SERVER_ERROR;
        this.details =
            Array.isArray(error.details) && error.name === constants_1.ERROR.NAME.VALIDATION
                ? simplifyJoiError(error.details)
                : error.details || null;
        this.originalError =
            error.originalError !== undefined ? error.originalError : null;
    }
    getError() {
        return {
            name: "Error",
            message: this.message,
            details: this.details
        };
    }
    getLogError() {
        let loggerMessageDetail;
        if (this.details) {
            loggerMessageDetail = `${JSON.stringify(this.details, null, "\t")}`;
        }
        else {
            if (!this.originalError) {
                loggerMessageDetail = "";
            }
            else {
                loggerMessageDetail = this.originalError.stack;
            }
        }
        return `${this.name} - ${this.message} ${loggerMessageDetail}`;
    }
}
exports.default = ClientError;
//# sourceMappingURL=client-error.js.map