"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const passport_jwt_1 = require("passport-jwt");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const crypto_1 = __importDefault(require("crypto"));
const client_error_1 = __importDefault(require("../client-error"));
const services_1 = require("../services");
const env_1 = __importDefault(require("../env"));
const constants_1 = require("../constants");
const authenticate = (req, payload, done) => {
    try {
        services_1.UserService.findOneById(payload.userId)
            .then((user) => {
            if (user === null) {
                done(new client_error_1.default({
                    name: constants_1.ERROR.NAME.AUTHENTICATION,
                    level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                    status: http_status_codes_1.default.NOT_FOUND,
                    message: constants_1.ERROR.USER.NOT_FOUND
                }), null);
            }
            else if (user.enabled !== true) {
                done(new client_error_1.default({
                    name: constants_1.ERROR.NAME.AUTHENTICATION,
                    level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO,
                    status: http_status_codes_1.default.UNAUTHORIZED,
                    message: constants_1.ERROR.USER.DISABLED
                }), null);
            }
            //req.user = user;
            done(null, user);
        })
            .catch(() => {
            done(new client_error_1.default({
                name: constants_1.ERROR.NAME.AUTHENTICATION,
                level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
                status: http_status_codes_1.default.NOT_FOUND,
                message: constants_1.ERROR.USER.NOT_FOUND
            }), null);
        });
    }
    catch (err) {
        done(err, null);
    }
};
const initPassportUse = () => {
    const opts = {
        jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: env_1.default.JWT_API_SECRET,
        passReqToCallback: true,
        algorithms: [env_1.default.JWT_ALGORITHM]
    };
    passport_1.default.use("jwt", new passport_jwt_1.Strategy(opts, authenticate));
};
const generateToken = (userId) => {
    let algorithm = constants_1.GLOBAL.SECURITY.DEFAULT_JWT_ALGORITHM;
    if (constants_1.GLOBAL.SECURITY.JWT_ALGORITHMS.includes(env_1.default.JWT_ALGORITHM)) {
        algorithm = env_1.default.JWT_ALGORITHM;
    }
    return jsonwebtoken_1.default.sign({
        userId
    }, env_1.default.JWT_API_SECRET, {
        algorithm: algorithm
    });
};
const hash = async (p) => {
    try {
        return await bcryptjs_1.default.hash(p, constants_1.GLOBAL.SECURITY.SALT_ROUNDS);
    }
    catch (error) {
        throw new client_error_1.default({
            name: constants_1.ERROR.NAME.AUTHENTICATION,
            message: error.message,
            level: constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR,
            status: http_status_codes_1.default.INTERNAL_SERVER_ERROR,
            originalError: error
        });
    }
};
const encrypt = (w, keyString = env_1.default.ENCRYPTION_KEY_DEFAULT) => {
    const key = Buffer.from(keyString);
    const iv = Buffer.alloc(constants_1.GLOBAL.SECURITY.ENCRYPT_IV_LENGTH, 0);
    const cipher = crypto_1.default.createCipheriv(constants_1.GLOBAL.SECURITY.ENCRYPT_ALGORITHM, key, iv);
    let encrypted = cipher.update(w, "utf8", "hex");
    encrypted += cipher.final("hex");
    return encrypted;
};
const decrypt = (w, keyString = env_1.default.ENCRYPTION_KEY_DEFAULT) => {
    const key = Buffer.from(keyString);
    const iv = Buffer.alloc(constants_1.GLOBAL.SECURITY.ENCRYPT_IV_LENGTH, 0);
    const decipher = crypto_1.default.createDecipheriv(constants_1.GLOBAL.SECURITY.ENCRYPT_ALGORITHM, key, iv);
    let decrypted = decipher.update(w, "hex", "utf8");
    decrypted += decipher.final("utf8");
    return decrypted;
};
const compare = async (clear, hashed) => {
    return await bcryptjs_1.default.compare(clear, hashed);
};
exports.default = {
    initPassportUse,
    generateToken,
    hash,
    compare,
    encrypt,
    decrypt
};
//# sourceMappingURL=security.helper.js.map