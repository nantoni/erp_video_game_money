"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fast_deep_equal_1 = __importDefault(require("fast-deep-equal"));
const checkArrayDuplicateItems = (items) => {
    for (let itemIndex = 0; itemIndex < items.length - 1; itemIndex++) {
        for (let itemCompareIndex = itemIndex + 1; itemCompareIndex < items.length; itemCompareIndex++) {
            if (fast_deep_equal_1.default(items[itemIndex], items[itemCompareIndex])) {
                return true;
            }
        }
    }
    return false;
};
exports.default = { checkArrayDuplicateItems };
//# sourceMappingURL=object.helper.js.map