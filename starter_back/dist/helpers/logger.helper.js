"use strict";
/*
 * Winston NPM Config levels:
 *  - error: number;
 *  - warn: number;
 *  - info: number;
 *  - http: number;
 *  - verbose: number;
 *  - debug: number;
 *  - silly: number;
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const rollbar_1 = __importDefault(require("rollbar"));
const winston_1 = require("winston");
const winston_transport_1 = __importDefault(require("winston-transport"));
const env_1 = __importDefault(require("../env"));
const constants_1 = require("../constants");
const rollbar = new rollbar_1.default({
    accessToken: env_1.default.ROLLBAR_KEY,
    captureUncaught: true,
    captureUnhandledRejections: true,
    enabled: env_1.default.NODE_ENV === constants_1.GLOBAL.ENV.PRODUCTION ||
        env_1.default.NODE_ENV === constants_1.GLOBAL.ENV.DEVELOPMENT
});
const logger = winston_1.createLogger({
    levels: winston_1.config.npm.levels,
    format: winston_1.format.json(),
    transports: [
        new winston_1.transports.Console({
            level: env_1.default.LOGGER_LEVEL_CONSOLE,
            format: winston_1.format.cli(),
            silent: env_1.default.NODE_ENV === constants_1.GLOBAL.ENV.TEST && !env_1.default.LOGGER_FORCE_TEST_CONSOLE
        })
    ]
});
logger.add(new winston_transport_1.default({
    level: env_1.default.LOGGER_LEVEL_ROLLBAR,
    handleExceptions: true,
    silent: env_1.default.NODE_ENV !== constants_1.GLOBAL.ENV.DEVELOPMENT &&
        env_1.default.NODE_ENV !== constants_1.GLOBAL.ENV.PRODUCTION,
    log: (args, next) => {
        switch (args.level) {
            case constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.DEBUG:
                rollbar.debug(args.message);
                break;
            case constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.INFO:
                rollbar.info(args.message);
                break;
            case constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.WARN:
                rollbar.warn(args.message);
                break;
            case constants_1.GLOBAL.LOGGER.LOGGER_LEVEL.ERROR:
                rollbar.error(args.message);
                break;
            default:
                rollbar.log(args.message);
        }
        return next();
    }
}));
exports.default = logger;
//# sourceMappingURL=logger.helper.js.map