"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const capitalizeOrNull = (s) => {
    if (!s) {
        return null;
    }
    return `${s.charAt(0).toUpperCase()}${s.slice(1)}`;
};
const capitalizeAllOrNull = (s) => {
    if (!s) {
        return null;
    }
    return s
        .split(" ")
        .map((word) => `${word.charAt(0).toUpperCase()}${word.substring(1)}`)
        .join(" ");
};
const toLowerCaseOrNull = (s) => {
    if (!s) {
        return null;
    }
    return s.toLowerCase();
};
const toUpperCaseOrNull = (s) => {
    if (!s) {
        return null;
    }
    return s.toUpperCase();
};
const forceToCase = (s, force) => {
    switch (force) {
        case "upper":
            return toUpperCaseOrNull(s);
        case "lower":
            return toLowerCaseOrNull(s);
        case "capitalize":
            return capitalizeOrNull(s);
        case "capitalize-all":
            return capitalizeAllOrNull(s);
        default:
            return null;
    }
};
exports.default = { forceToCase };
//# sourceMappingURL=string.helper.js.map