"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSchemeByRole = exports.serializeMany = exports.serialize = void 0;
const constants_1 = require("../constants");
const include = (includeScheme, res, data) => {
    for (const attribute of includeScheme) {
        if (data[attribute] !== undefined) {
            res[attribute] = data[attribute];
        }
    }
    return res;
};
const exclude = (excludeScheme, res) => {
    for (const attribute of excludeScheme) {
        if (res[attribute] !== undefined) {
            delete res[attribute];
        }
    }
    return res;
};
const serialize = (data, scheme) => {
    let res = {};
    const dataAttributes = Object.prototype.hasOwnProperty.call(data, "dataValues")
        ? data.get({ plain: true })
        : data; // check if sequelize object
    res = include(scheme.include, res, dataAttributes);
    if (scheme.exclude !== undefined) {
        res = exclude(scheme.exclude, res);
    }
    if (Object.prototype.hasOwnProperty.call(scheme, "assoc")) {
        for (const assoc in scheme.assoc) {
            if (Object.prototype.hasOwnProperty.call(dataAttributes, assoc)) {
                if (Array.isArray(dataAttributes[assoc])) {
                    // HasMany
                    const associations = [];
                    for (const item of dataAttributes[assoc]) {
                        associations.push(serialize(item, scheme.assoc[assoc]));
                    }
                    Object.assign(res, {
                        [assoc]: associations
                    });
                }
                else {
                    // HasOne or BelongsTo
                    if (dataAttributes[assoc]) {
                        Object.assign(res, {
                            [assoc]: serialize(dataAttributes[assoc], scheme.assoc[assoc])
                        });
                    }
                }
            }
        }
    }
    return res;
};
exports.serialize = serialize;
const serializeMany = (data, scheme) => {
    const res = [];
    for (const item of data) {
        res.push(serialize(item, scheme));
    }
    return res;
};
exports.serializeMany = serializeMany;
const getSchemeByRole = (SCHEMES, scheme, role = constants_1.USER.ROLE.ENUM.USER) => {
    if (role === constants_1.USER.ROLE.ENUM.ADMIN &&
        Object.prototype.hasOwnProperty.call(SCHEMES.ADMIN, scheme)) {
        return SCHEMES.ADMIN[scheme];
    }
    else {
        return SCHEMES.DEFAULT[scheme];
    }
};
exports.getSchemeByRole = getSchemeByRole;
//# sourceMappingURL=scheme.helper.js.map