"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const express_1 = __importStar(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
const helmet_1 = __importDefault(require("helmet"));
const morgan_1 = __importDefault(require("morgan"));
const express_async_handler_1 = __importDefault(require("express-async-handler"));
const express_rate_limit_1 = __importDefault(require("express-rate-limit"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const env_1 = __importDefault(require("./env"));
const client_error_1 = __importDefault(require("./client-error"));
const constants_1 = require("./constants");
const swagger_1 = __importDefault(require("./swagger"));
const middlewares_1 = require("./middlewares");
const logger_helper_1 = __importDefault(require("./helpers/logger.helper"));
const routes_1 = __importDefault(require("./routes"));
class App {
    constructor(port) {
        this.app = express_1.default();
        this.router = express_1.Router();
        this.port = port;
        this.initSwagger();
        this.initMiddlewares();
        this.initRateLimiterMiddleware();
        this.initRoutes();
        this.initErrorMiddleware();
    }
    get instance() {
        return this.app;
    }
    listen() {
        this.server = http_1.default.createServer(this.app);
        this.server.listen(this.port);
        console.log(`App listening on port ${this.port}`);
    }
    close() {
        this.server.close();
    }
    initSwagger() {
        if (env_1.default.NODE_ENV && env_1.default.NODE_ENV === "local") {
            const swaggerBuild = swagger_1.default();
            this.app.use("/api-docs", swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swaggerBuild));
            this.router.get("/api-docs", swagger_ui_express_1.default.setup(swaggerBuild));
        }
    }
    initMiddlewares() {
        this.app
            .use(cors_1.default())
            .use(helmet_1.default())
            .use(body_parser_1.default.urlencoded({ limit: "15mb", extended: false }))
            .use(body_parser_1.default.json({ limit: "15mb" }))
            .use(compression_1.default())
            .use(morgan_1.default(":method :url :status :remote-addr :response-time ms - :res[content-length]", {
            stream: {
                write: (message) => {
                    logger_helper_1.default.info(message);
                }
            }
        }))
            .use(middlewares_1.maintenanceMiddleware());
    }
    initRateLimiterMiddleware() {
        if (env_1.default.NODE_ENV !== constants_1.GLOBAL.ENV.TEST) {
            const windowMs = parseInt(env_1.default.RATE_LIMITER_WINDOW_MS, 10);
            const max = parseInt(env_1.default.RATE_LIMITER_MAX_COUNT, 10);
            const limiter = express_rate_limit_1.default({
                windowMs,
                max
            });
            this.app.use(limiter);
        }
    }
    initErrorMiddleware() {
        this.app
            .use(middlewares_1.errorMiddleware.sequelizeErrorHandler)
            .use(middlewares_1.errorMiddleware.otherErrorHandler)
            .use(middlewares_1.errorMiddleware.logErrors)
            .use(middlewares_1.errorMiddleware.errorHandler);
    }
    initRoutes() {
        routes_1.default.map((route) => {
            const path = route.path;
            this.router.use(`/${path}`, route.router);
        });
        this.app.use("/v1", this.router);
        // 404 handler (ALWAYS keep this as the last route)
        this.app.all("*", express_async_handler_1.default((req, res) => {
            const originalUrl = req.originalUrl;
            const method = req.method;
            const error = new client_error_1.default({
                name: constants_1.ERROR.NAME.ROUTER,
                message: `Route ${originalUrl} not found with ${method} method`,
                status: http_status_codes_1.default.NOT_FOUND
            });
            res.status(http_status_codes_1.default.NOT_FOUND).json({ error: error.getError() });
        }));
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map