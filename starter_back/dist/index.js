"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const env_1 = __importDefault(require("./env"));
const models_1 = require("./models");
const init = async () => {
    models_1.initModels();
    await models_1.syncDatabase();
};
const port = env_1.default.PORT;
const app = new app_1.default(port);
init()
    .then(() => {
    app.listen();
})
    .catch((err) => {
    console.log(`${err.name} : ${err.message}
      ${err.stack || ""}`);
    throw err;
});
//# sourceMappingURL=index.js.map