"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const base_service_1 = __importDefault(require("./base.service"));
const models_1 = require("../models");
class InvoiceAuxiliarRowService extends base_service_1.default {
    static async getBankTransactions() {
        const transactions = await this.findAll({
            where: {
                debit: {
                    [sequelize_1.Op.ne]: null
                }
            },
            limit: 100
        });
        const res = [];
        for (const transaction of transactions) {
            res.push({
                date: transaction.date,
                ref: transaction.invoiceId,
                description: `Prélèvement Client n°${transaction.clientId}`,
                montant: transaction.debit
            });
        }
        return res;
    }
    static async createInvoiceAuxiliarRow(rowBody) {
        return await this.create(rowBody);
    }
    static async findOneById(id) {
        return await this.findOne({
            invoiceId: id
        });
    }
    static async findAllRaws(options) {
        return await this.findAll(options);
    }
    static async findAllByInvoiceId(id) {
        return await this.findAll({
            where: {
                invoiceId: id
            }
        });
    }
    static async countAllRaws(options) {
        return await this.count(options);
    }
    static async findOne(optionsWhere, optionsOrder = [["date", "DESC"]]) {
        return models_1.InvoiceAuxiliarRow.findOne({
            where: Object.assign({}, optionsWhere),
            order: optionsOrder
        });
    }
    static async findAll(options, optionsOrder = [["date", "DESC"]]) {
        return models_1.InvoiceAuxiliarRow.findAll(Object.assign(Object.assign({}, options), { order: optionsOrder }));
    }
    static async create(invoiceValues, options) {
        return models_1.InvoiceAuxiliarRow.create(invoiceValues, options);
    }
    static async count(options) {
        return models_1.InvoiceAuxiliarRow.count(options);
    }
}
exports.default = InvoiceAuxiliarRowService;
//# sourceMappingURL=invoice-auxiliar-row.service.js.map