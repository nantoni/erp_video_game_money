"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DashboardService = exports.InvoiceAuxiliarRowService = exports.InvoiceService = void 0;
const invoice_service_1 = __importDefault(require("./invoice.service"));
exports.InvoiceService = invoice_service_1.default;
const invoice_auxiliar_row_service_1 = __importDefault(require("./invoice-auxiliar-row.service"));
exports.InvoiceAuxiliarRowService = invoice_auxiliar_row_service_1.default;
const dashboard_service_1 = __importDefault(require("./dashboard.service"));
exports.DashboardService = dashboard_service_1.default;
//# sourceMappingURL=index.js.map