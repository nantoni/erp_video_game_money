"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = __importDefault(require("./base.service"));
const models_1 = require("../models");
const client_error_1 = __importDefault(require("../client-error"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const constants_1 = require("../constants");
const invoice_auxiliar_row_service_1 = __importDefault(require("./invoice-auxiliar-row.service"));
class InvoiceService extends base_service_1.default {
    static async createInvoice(invoiceBody) {
        const countInvoices = await this.count({
            where: { invoiceId: invoiceBody.invoiceId }
        });
        if (countInvoices > 0) {
            throw new client_error_1.default({
                name: constants_1.ERROR.NAME.VALIDATION,
                message: constants_1.ERROR.INVOICE.ALREADY_EXISTS,
                status: http_status_codes_1.default.CONFLICT
            });
        }
        const vat = invoiceBody.VAT !== undefined ? invoiceBody.VAT : 20;
        const invoiceRaws = [
            {
                date: invoiceBody.date,
                account: constants_1.INVOICE.ACCOUNT.VAT.ID,
                description: constants_1.INVOICE.ACCOUNT.VAT.DESCIPTION,
                invoiceId: invoiceBody.invoiceId,
                debit: null,
                credit: (invoiceBody.amount * vat) / 100
            },
            {
                date: invoiceBody.date,
                account: constants_1.INVOICE.ACCOUNT.PRODUCT.ID,
                description: constants_1.INVOICE.ACCOUNT.PRODUCT.DESCIPTION,
                invoiceId: invoiceBody.invoiceId,
                debit: null,
                credit: invoiceBody.amount - (invoiceBody.amount * vat) / 100
            },
            {
                date: invoiceBody.date,
                account: constants_1.INVOICE.ACCOUNT.PRODUCT.ID,
                description: constants_1.INVOICE.ACCOUNT.PRODUCT.DESCIPTION,
                invoiceId: invoiceBody.invoiceId,
                debit: invoiceBody.amount,
                credit: null
            }
        ];
        const auxiliarRaw = {
            invoiceId: invoiceBody.invoiceId,
            clientId: invoiceBody.clientId,
            date: invoiceBody.date,
            debit: null,
            credit: invoiceBody.amount
        };
        await invoice_auxiliar_row_service_1.default.createInvoiceAuxiliarRow(auxiliarRaw);
        return await this.createMany(invoiceRaws);
    }
    static async findOneById(id) {
        return await this.findOne({
            invoiceId: id
        });
    }
    static async findAllRaws(options) {
        return await this.findAll(options);
    }
    static async countAllRaws(options) {
        return await this.count(options);
    }
    static async findAllByInvoiceId(id) {
        return await this.findAll({
            where: { invoiceId: id }
        });
    }
    static async findOne(optionsWhere, optionsOrder = [["date", "DESC"]]) {
        return models_1.InvoiceRaw.findOne({
            where: Object.assign({}, optionsWhere),
            order: optionsOrder
        });
    }
    static async findAll(options, optionsOrder = [["date", "DESC"]]) {
        return models_1.InvoiceRaw.findAll(Object.assign(Object.assign({}, options), { order: optionsOrder }));
    }
    static async create(invoiceValues, options) {
        return models_1.InvoiceRaw.create(invoiceValues, options);
    }
    static async createMany(invoiceValues, options) {
        return models_1.InvoiceRaw.bulkCreate(invoiceValues, options);
    }
    static async count(options) {
        return models_1.InvoiceRaw.count(options);
    }
}
exports.default = InvoiceService;
//# sourceMappingURL=invoice.service.js.map