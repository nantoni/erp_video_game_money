"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = __importDefault(require("./base.service"));
class InvoiceService extends base_service_1.default {
    static async findOneById(id) {
        return await this.findOne({
            invoiceId: id
        });
    }
    static async findOne(optionsWhere, optionsOrder = [["date", "DESC"]]) {
        return Invoice.findOne({
            where: Object.assign({}, optionsWhere),
            order: optionsOrder
        });
    }
    static async create(invoiceValues, options) {
        return Invoice.create(invoiceValues, options);
    }
    static async count(options) {
        return Invoice.count(options);
    }
}
exports.default = InvoiceService;
//# sourceMappingURL=invoice-raw.service.js.map