"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const constants_1 = require("../constants");
const date_fns_1 = require("date-fns");
const base_service_1 = __importDefault(require("./base.service"));
const models_1 = require("../models");
const invoice_service_1 = __importDefault(require("./invoice.service"));
const invoice_auxiliar_row_service_1 = __importDefault(require("./invoice-auxiliar-row.service"));
class DashboardService extends base_service_1.default {
    static async getWeeklyIncomes() {
        const weeklyIncomes = await invoice_service_1.default.findAllRaws({
            where: {
                account: constants_1.INVOICE.ACCOUNT.PRODUCT.ID,
                debit: { [sequelize_1.Op.ne]: null },
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 7), "yyyy-MM-dd"),
                        date_fns_1.format(new Date(), "yyyy-MM-dd")
                    ]
                }
            },
            attributes: ["debit"]
        });
        const oldClients = await invoice_auxiliar_row_service_1.default.findAllRaws({
            where: {
                date: {
                    [sequelize_1.Op.lt]: [date_fns_1.format(date_fns_1.subDays(new Date(), 7), "yyyy-MM-dd")]
                }
            },
            attributes: ["clientId"]
        });
        const oldClientIds = oldClients.map((row) => {
            return row.clientId;
        });
        const newWeeklyClient = await invoice_auxiliar_row_service_1.default.findAllRaws({
            where: {
                clientId: {
                    [sequelize_1.Op.notIn]: oldClientIds
                }
            }
        });
        const newClients = newWeeklyClient.map((row) => {
            return row.clientId;
        });
        const resClient = [];
        for (const newClient of newClients) {
            if (!resClient.includes(newClient)) {
                resClient.push(newClient);
            }
        }
        const weeklyOrdersCount = await invoice_auxiliar_row_service_1.default.countAllRaws({
            where: {
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 7), "yyyy-MM-dd"),
                        date_fns_1.format(date_fns_1.addDays(new Date(), 1), "yyyy-MM-dd")
                    ]
                },
                credit: { [sequelize_1.Op.ne]: null }
            }
        });
        const weeklyUnresolvedOrdersCount = await invoice_auxiliar_row_service_1.default.findAllRaws({
            where: {
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 7), "yyyy-MM-dd"),
                        date_fns_1.format(date_fns_1.addDays(new Date(), 1), "yyyy-MM-dd")
                    ]
                },
                credit: { [sequelize_1.Op.ne]: null }
            }
        });
        const weeklyResolvedOrdersCount = await invoice_auxiliar_row_service_1.default.findAllRaws({
            where: {
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 7), "yyyy-MM-dd"),
                        date_fns_1.format(date_fns_1.addDays(new Date(), 1), "yyyy-MM-dd")
                    ]
                },
                debit: { [sequelize_1.Op.ne]: null }
            }
        });
        const unresolved = weeklyUnresolvedOrdersCount.reduce((a, b) => a + b.credit, 0);
        const resolved = weeklyResolvedOrdersCount.reduce((a, b) => a + b.debit, 0);
        return {
            chiffreAffaire: weeklyIncomes !== null
                ? weeklyIncomes.reduce((a, b) => a + b.debit, 0)
                : 0,
            nouveauxClients: resClient.length,
            nbCommandes: weeklyOrdersCount,
            enAttente: unresolved - resolved
        };
    }
    static async getMonthlyIncomes() {
        const monthlyIncomes = await invoice_service_1.default.findAllRaws({
            where: {
                account: constants_1.INVOICE.ACCOUNT.PRODUCT.ID,
                debit: { [sequelize_1.Op.ne]: null },
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 31), "yyyy-MM-dd"),
                        date_fns_1.format(date_fns_1.addDays(new Date(), 1), "yyyy-MM-dd")
                    ]
                }
            },
            attributes: ["debit"]
        });
        const yearlyIncomes = await invoice_service_1.default.findAllRaws({
            where: {
                account: constants_1.INVOICE.ACCOUNT.PRODUCT.ID,
                debit: { [sequelize_1.Op.ne]: null },
                date: {
                    [sequelize_1.Op.between]: [
                        date_fns_1.format(date_fns_1.subDays(new Date(), 365), "yyyy-MM-dd"),
                        date_fns_1.format(date_fns_1.addDays(new Date(), 1), "yyyy-MM-dd")
                    ]
                }
            },
            attributes: ["debit", "date"]
        });
        const monthlyResults = new Array(12).fill(0);
        for (const income of yearlyIncomes) {
            monthlyResults[income.date.getMonth()] += income.debit;
        }
        return {
            revenu: monthlyIncomes !== null
                ? monthlyIncomes.reduce((a, b) => a + b.debit, 0)
                : 0,
            revenuMois: monthlyResults
        };
    }
    static async getNewClientsFigures() {
        const monthlyClients = new Array(12).fill(0);
        for (let i = 0; i < 12; i++) {
            const date = new Date();
            const oldClients = await invoice_auxiliar_row_service_1.default.findAllRaws({
                where: {
                    date: {
                        [sequelize_1.Op.lt]: date_fns_1.format(date_fns_1.subMonths(new Date(date.getFullYear(), date.getMonth(), 1), i), "yyyy-MM-dd")
                    }
                }
            });
            const newClients = await invoice_auxiliar_row_service_1.default.findAllRaws({
                where: {
                    clientId: {
                        [sequelize_1.Op.notIn]: oldClients.map((row) => {
                            return row.clientId;
                        })
                    },
                    date: {
                        [sequelize_1.Op.between]: [
                            date_fns_1.format(date_fns_1.subMonths(new Date(date.getFullYear(), date.getMonth(), 1), i), "yyyy-MM-dd"),
                            date_fns_1.format(date_fns_1.subMonths(new Date(date.getFullYear(), date.getMonth() + 1, 0), i), "yyyy-MM-dd")
                        ]
                    }
                }
            });
            const array = [];
            for (const newClient of newClients) {
                if (!array.includes(newClient.clientId)) {
                    array.push(newClient.clientId);
                }
            }
            monthlyClients[i] = array.length;
        }
        return {
            nbNouveauxClients: monthlyClients[0] + 8,
            nbNouveauxClientsMois: monthlyClients
        };
    }
    static async getTopClientsFigures() {
        const counts = await models_1.sequelize.query("SELECT DISTINCT(client_id) as idClient, COUNT(*) as nbCommandes FROM invoice_auxiliar_raws GROUP BY client_id ORDER BY nbCommandes DESC LIMIT 5");
        return counts[0];
    }
    static async getLastActivities() {
        const activitiesCount = await invoice_auxiliar_row_service_1.default.countAllRaws({
            where: { credit: { [sequelize_1.Op.ne]: null } }
        });
        const activities2 = await models_1.sequelize.query("SELECT client_id as idClient, date, credit as montantCommande FROM invoice_auxiliar_raws WHERE credit IS NOT NULL ORDER BY date DESC LIMIT 5");
        return {
            nbVentes: activitiesCount,
            data: activities2[0]
        };
    }
}
exports.default = DashboardService;
//# sourceMappingURL=dashboard.service.js.map