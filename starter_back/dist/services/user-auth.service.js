"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const security_helper_1 = __importDefault(require("../helpers/security.helper"));
const user_service_1 = __importDefault(require("./user.service"));
const base_service_1 = __importDefault(require("./base.service"));
const client_error_1 = __importDefault(require("../client-error"));
const constants_1 = require("../constants");
const { LOGGER } = constants_1.GLOBAL;
const http_status_codes_1 = __importDefault(require("http-status-codes"));
class UserAuthService extends base_service_1.default {
    static async login(values) {
        const user = await user_service_1.default.findOneByEmail(values.email);
        if (user === null) {
            throw new client_error_1.default({
                name: constants_1.ERROR.NAME.AUTHENTICATION,
                level: LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.UNAUTHORIZED,
                message: constants_1.ERROR.AUTHENTICATION.UNAUTHORIZED
            });
        }
        if (user.enabled === false) {
            throw new client_error_1.default({
                name: constants_1.ERROR.NAME.AUTHENTICATION,
                level: LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.FORBIDDEN,
                message: constants_1.ERROR.AUTHENTICATION.FORBIDDEN
            });
        }
        const passwordMatch = await security_helper_1.default.compare(values.password, user.password);
        if (!passwordMatch) {
            throw new client_error_1.default({
                name: constants_1.ERROR.NAME.AUTHENTICATION,
                level: LOGGER.LOGGER_LEVEL.INFO,
                status: http_status_codes_1.default.FORBIDDEN,
                message: constants_1.ERROR.AUTHENTICATION.FORBIDDEN
            });
        }
        return user;
    }
}
/*
const passwordResetRequest = async (
  values: PasswordResetTokenCreate
): Promise<PasswordResetToken> => {
  const passwordResetToken: PasswordResetToken = await passwordResetTokenRepository.create(
    values
  );

  const user = await findOne({
    userId: passwordResetToken.userId
  });

  if (
    ENV.NODE_ENV === GLOBAL.ENV.PRODUCTION ||
    ENV.NODE_ENV === GLOBAL.ENV.DEVELOPMENT
  ) {
    const templateId: number = parseInt(
      ENV.SENDINBLUE_TEMPLATE_ID_PASSWORD_RESET,
      10
    );
    try {
      await sendinblue.sendMail(user.email, templateId, {
        TOKEN: passwordResetToken.token
      });
    } catch (err) {
      // log detailed sendinblue error
      if (err.response !== undefined && err.response.error !== undefined) {
        Logger.error(
          new ClientError({
            name: ERROR.NAME.EMAIL,
            message: ERROR.EMAIL.SEND_FAILURE,
            details: err.response.error
          }).getLogError()
        );
      }

      // throw email failure error for the client
      throw new ClientError({
        name: ERROR.NAME.EMAIL,
        message: ERROR.EMAIL.SEND_FAILURE
      });
    }
  }

  return passwordResetToken;
};

const passwordResetTokenFindOne = async (
  token: string
): Promise<PasswordResetToken | null> => {
  const option = new PasswordResetTokenFindOneOptions({ token });
  const query = option.build();
  return await passwordResetTokenRepository.findOne(query);
};

const passwordResetTokenSetAsUsed = async (
  passwordResetToken: PasswordResetToken
): Promise<void> => {
  const option = new PasswordResetTokenFindOneOptions({
    token: passwordResetToken.token
  });
  const query = option.build();

  await passwordResetTokenRepository.update(
    { used: true },
    {
      where: query.where
    }
  );
};
*/
exports.default = UserAuthService;
//# sourceMappingURL=user-auth.service.js.map