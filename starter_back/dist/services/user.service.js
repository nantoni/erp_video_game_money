"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const base_service_1 = __importDefault(require("./base.service"));
const models_1 = require("../models");
const client_error_1 = __importDefault(require("../client-error"));
const http_status_codes_1 = __importDefault(require("http-status-codes"));
const constants_1 = require("../constants");
class UserService extends base_service_1.default {
    static async createUser(userBody) {
        const countUsers = await this.count({
            where: { invoiceId: userBody.invoiceId }
        });
        if (countUsers > 0) {
            throw new client_error_1.default({
                name: constants_1.ERROR.NAME.VALIDATION,
                message: constants_1.ERROR.INVOICE.ALREADY_EXISTS,
                status: http_status_codes_1.default.CONFLICT
            });
        }
        return await this.create(userBody);
    }
    static async findOneById(id) {
        return await this.findOne({
            invoiceId: id
        });
    }
    static async findOne(optionsWhere, optionsOrder = [["date", "DESC"]]) {
        return models_1.User.findOne({
            where: Object.assign({}, optionsWhere),
            order: optionsOrder
        });
    }
    static async create(userValues, options) {
        return models_1.User.create(userValues, options);
    }
    static async count(options) {
        return models_1.User.count(options);
    }
}
exports.default = UserService;
//# sourceMappingURL=user.service.js.map