"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cron_1 = __importDefault(require("cron"));
const axios_1 = __importDefault(require("axios"));
const invoice_auxiliar_row_service_1 = __importDefault(require("./services/invoice-auxiliar-row.service"));
const logger_helper_1 = __importDefault(require("./helpers/logger.helper"));
const models_1 = require("./models");
const sequelize_1 = require("sequelize");
// destroy all old consumer logbook infos, depending days count limit provided in .ENV
new cron_1.default.CronJob({
    cronTime: "0 */5 * * * *",
    onTick: async function () {
        try {
            models_1.initModels();
            const bankTransactions = await invoice_auxiliar_row_service_1.default.findAllRaws({
                where: {
                    debit: { [sequelize_1.Op.ne]: null }
                }
            });
            const bankTransactionsIds = bankTransactions.map((transaction) => {
                return transaction.invoiceId;
            });
            const clientTransactions = await invoice_auxiliar_row_service_1.default.findAllRaws({
                where: {
                    credit: { [sequelize_1.Op.ne]: null },
                    invoiceId: { [sequelize_1.Op.notIn]: bankTransactionsIds }
                }
            });
            const createdBankTransactions = [];
            for (const clientTransaction of clientTransactions) {
                const bankTransaction = await invoice_auxiliar_row_service_1.default.createInvoiceAuxiliarRow({
                    invoiceId: clientTransaction.invoiceId,
                    clientId: clientTransaction.clientId,
                    date: new Date(),
                    debit: clientTransaction.credit,
                    credit: null
                });
                createdBankTransactions.push({
                    invoiceId: bankTransaction.invoiceId,
                    date: bankTransaction.date
                });
            }
            if (createdBankTransactions.length > 0) {
                logger_helper_1.default.debug("CRON - POST validations to Video Games group - DATE : ", new Date());
                await axios_1.default.post("http://cnamerp.leojullerot.fr:4689/setPayments", createdBankTransactions);
            }
            logger_helper_1.default.debug("CRON - Bank has validated transactions - DATE : ", new Date());
        }
        catch (err) {
            logger_helper_1.default.error("CRON - ERROR in Bank valiadations : ", err);
        }
    },
    start: true,
    timeZone: "Europe/Paris"
});
//# sourceMappingURL=cron.js.map